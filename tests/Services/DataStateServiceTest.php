<?php

namespace Tests\Services;

use Lantana\Models\JSBisqueConfigs;
use Lantana\Models\JSSharedAmocrm;
use Lantana\Models\JSSharedModules;
use Lime\Modules\Anemone\Module as AnemoneModule;
use Lime\Modules\Crocus\Module as CrocusModule;
use Lime\Services\DataStateService;
use PHPUnit\Framework\TestCase;

class DataStateServiceTest extends TestCase
{
    protected ?DataStateService $service;
    protected string $uriJS = 'https://jstorage.local';
    protected string $apiKeyJS = 'fd537acfd1bf456185123709b952ce4b';

    public function test__construct()
    {
        $instance = new DataStateService(collect());
        $this->assertInstanceOf(DataStateService::class, $instance);
    }

    public function testGetForModule()
    {
        $this->service->setForModule('anemone_uuid', 'leads', collect([1, 2, 3]));
        $this->service->initDeepData(5);
        $this->service->setForModule('crocus_uuid', 'any', ['one', 'two'], 3);
        $this->service->setForModule('crocus_uuid', 'any_2', 'free', 3);
        $this->service->setForModule('crocus_uuid', 'any_2', ['one', 'two', 'next'], 4);

        $data1 = $this->service->getForModule('anemone_uuid', 'leads');
        $this->assertCount(3, $data1);

        $data2 = $this->service->getForModule('crocus_uuid', 'not_found', 1);
        $this->assertNull($data2);

        $data3 = $this->service->getForModule('crocus_uuid', 'any', 3);
        $this->assertCount(2, $data3);
        $this->assertContains('two', $data3);
        $this->assertEquals('one', $data3[0]);

        $data4 = $this->service->getForModule('crocus_uuid', 'any_2', 3);
        $this->assertEquals('free', $data4);

        $data5 = $this->service->getForModule('crocus_uuid', 'any_2', 4);
        $this->assertCount(3, $data5);

        $data5 = $this->service->getForModule('crocus_uuid', 'any_2', 5);
        $this->assertNull($data5);
    }

    public function testSetForModule()
    {
        $this->service->setForModule('anemone_uuid', 'leads', collect([1, 2, 3]));

        $this->service->initDeepData(1);
        $this->service->setForModule('anemone_uuid', 'contacts', collect([1, 2, 3, 4, 5]), 1);

        $data1 = $this->service->getForModule('anemone_uuid', 'leads');

        $this->assertCount(3, $data1);

        $data2 = $this->service->getForModule('anemone_uuid', 'contacts', 1);

        $this->assertCount(5, $data2);
        $this->assertEquals(1, $data2->first());
    }

    public function testGetPullDataForModule()
    {
        $this->service->initDeepData(5);

        $this->service->initPullData(0);
        $this->service->setPullDataForModule('anemone_uuid', 'leads', collect([1, 2, 3]));
        $this->service->initPullData(3);
        $this->service->setPullDataForModule('crocus_uuid', 'any', ['one', 'two'], 3);
        $this->service->setPullDataForModule('crocus_uuid', 'any_2', 'free', 3);
        $this->service->initPullData(4);
        $this->service->setPullDataForModule('crocus_uuid', 'any_2', ['one', 'two', 'next'], 4);

        $data1 = $this->service->getPullDataForModule('anemone_uuid', 'leads');
        $this->assertCount(3, $data1);
        $this->service->destroyPullData(0);
        $data11 = $this->service->getPullDataForModule('anemone_uuid', 'leads');
        $this->assertNull($data11);

        $this->service->initPullData(1);
        $data2 = $this->service->getPullDataForModule('crocus_uuid', 'not_found', 1);
        $this->assertNull($data2);
        $this->service->destroyPullData(1);

        $data3 = $this->service->getPullDataForModule('crocus_uuid', 'any', 3);
        $this->assertCount(2, $data3);
        $this->assertContains('two', $data3);
        $this->assertEquals('one', $data3[0]);

        $data4 = $this->service->getPullDataForModule('crocus_uuid', 'any_2', 3);
        $this->assertEquals('free', $data4);

        $data5 = $this->service->getPullDataForModule('crocus_uuid', 'any_2', 4);
        $this->assertCount(3, $data5);

        $data5 = $this->service->getPullDataForModule('crocus_uuid', 'any_2', 5);
        $this->assertNull($data5);
    }

    public function testSetPullDataForModule()
    {
        $this->service->initPullData(0);
        $this->service->setPullDataForModule('anemone_uuid', 'leads', collect([1, 2, 3]));

        $data1 = $this->service->getPullDataForModule('anemone_uuid', 'leads');
        $this->assertCount(3, $data1);
        $this->service->destroyPullData(0);
        $data11 = $this->service->getPullDataForModule('anemone_uuid', 'leads');
        $this->assertNull($data11);
    }

    public function testInitPullData()
    {
        $this->service->initDeepData(4);
        $this->service->initPullData(3);
        $this->service->setPullDataForModule('anemone_uuid', 'leads', collect([1, 2, 3]), 3);

        $data1 = $this->service->getPullDataForModule('anemone_uuid', 'leads', 3);
        $this->assertCount(3, $data1);
        $this->service->destroyPullData(0);
        $data11 = $this->service->getPullDataForModule('anemone_uuid', 'leads');
        $this->assertNull($data11);
    }

    public function testDestroyPullData()
    {
        $this->service->initDeepData(4);
        $this->service->initPullData(3);
        $this->service->setPullDataForModule('anemone_uuid', 'leads', collect([1, 2, 3]), 3);
        $this->service->setPullDataForModule('anemone_uuid', 'leads', collect([1, 2, 3]), 4);

        $data1 = $this->service->getPullDataForModule('anemone_uuid', 'leads', 3);
        $data2 = $this->service->getPullDataForModule('anemone_uuid', 'leads', 4);
        $this->assertCount(3, $data1);
        $this->assertNull($data2);
        $this->service->destroyPullData(3);
        $data11 = $this->service->getPullDataForModule('anemone_uuid', 'leads');
        $this->assertNull($data11);
    }

    public function testInitDeepData()
    {
        $this->service->setForModule('anemone_uuid', 'leads', collect([1, 2, 3]), 2);

        $this->service->initDeepData(3);
        $this->service->setForModule('anemone_uuid', 'contacts', collect([1, 2, 3, 4, 5]), 3);

        $data1 = $this->service->getForModule('anemone_uuid', 'leads', 2);
        $data2 = $this->service->getForModule('anemone_uuid', 'contacts', 3);

        $this->assertNull($data1);
        $this->assertCount(5, $data2);
    }

    public function testDestroyDeepData()
    {
        $this->service->setForModule('anemone_uuid', 'leads', collect([1, 2, 3]), 2);

        $this->service->initDeepData(3);
        $this->service->setForModule('anemone_uuid', 'contacts', collect([1, 2, 3, 4, 5]), 3);

        $data1 = $this->service->getForModule('anemone_uuid', 'leads', 2);
        $data2 = $this->service->getForModule('anemone_uuid', 'contacts', 3);

        $this->assertNull($data1);
        $this->assertCount(5, $data2);

        $this->service->destroyDeepData(3);

        $data22 = $this->service->getForModule('anemone_uuid', 'contacts', 3);
        $this->assertNull($data22);
    }

    public function testList()
    {
        $this->assertTrue(true);
    }

    public function testGetDataSkeleton()
    {
        $this->assertTrue(true);
    }

    public function testGetModules()
    {
        $this->assertTrue(true);
    }

    protected function setUp(): void
    {
        $this->service = new DataStateService(
            collect(
                [
                    new AnemoneModule(
                        new JSSharedModules($this->uriJS, $this->apiKeyJS, ['uuid' => 'anemone_uuid']),
                        new JSSharedAmocrm($this->uriJS, $this->apiKeyJS)
                    ),
                    new CrocusModule(
                        new JSSharedModules($this->uriJS, $this->apiKeyJS, ['uuid' => 'crocus_uuid']),
                        new JSBisqueConfigs($this->uriJS, $this->apiKeyJS)
                    ),
                ]
            )
        );
    }

    protected function tearDown(): void
    {
        $this->service = null;
    }
}
