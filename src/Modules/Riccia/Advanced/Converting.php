<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.25
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Riccia\Advanced;

use Lime\Contracts\BeConverting;
use Lime\Contracts\BeExecuteConvert;
use Lime\Helpers\DeepEntity;
use Lime\Services\DataStateService;

class Converting implements BeConverting
{
    use DeepEntity;

    private DataStateService $dataStateService;

    /**
     * @param DataStateService $dataStateService
     */
    public function __construct(DataStateService $dataStateService)
    {
        $this->dataStateService = $dataStateService;
    }

    /**
     * @inheritDoc
     */
    public function getValue($bottomValue, array $deep)
    {
        return null;
    }

    /**
     * @inheritDoc
     * */
    public function getPreparingData($bottomValue, array $deep)
    {
        if (strpos($bottomValue->prop->id, 'entities') === 0) {
            $switch = 'item_iterator';
            $bottomValue->prop->value = (object)['top_code' => $bottomValue->prop->top_code];
            $injectEntity = $this->getDeepEntity($deep, (object)['prop' => (object)['id' => 'item_iterator']]);

            return [$switch, $injectEntity];
        } elseif (strpos($bottomValue->prop->id, 'collection') === 0) {
            $switch = 'system_collection';
            $injectEntity = $this->getDeepEntity($deep, (object)['prop' => (object)['id' => 'system_collection']]);

            return [$switch, $injectEntity];
        }

        throw new \Exception('Not prepare');
    }

    /**
     * @inheritDoc
     * */
    public function getConvert(string $code): ?BeExecuteConvert
    {
        return null;
    }

}
