<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.14
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Riccia\Advanced;

trait Controlling
{
    /**
     * @param mixed $one
     * @param array $inject
     * @return array (first alien module, second value from module)
     */
    protected function getMixedAlien($one, array $inject)
    {
        if (!$this->isControlValues($one)) {
            return [null, null];
        }

        $alienModule = $this->getAlienModule($one);

        return [$alienModule, $this->getAlienValue($alienModule, $one, $inject)];
    }

    /**
     * @param mixed $one
     * @return bool
     * */
    protected function isControlValues($one)
    {
        return isset($one->values->topValue, $one->values->bottomValue);
    }

    /**
     * @param mixed $one
     * @return \Lime\Contracts\BeModule|null
     * */
    protected function getAlienModule($one)
    {
        return $this->modulesService->getModule($one->values->bottomValue->module->uuid) ?? null;
    }

    /**
     * @param \Lime\Contracts\BeModule|null $alienModule
     * @param mixed $one
     * @param array $inject
     * @return mixed|null
     */
    protected function getAlienValue($alienModule, $one, array $inject)
    {
        if (empty($alienModule)) {
            return null;
        }

        $alienValue = $alienModule->getValueFor($this->dataStateService, $one->values->bottomValue, $inject);
        return empty($alienValue) ? null : $alienValue;
    }
}
