<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.14
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Riccia\Actions;

use Lime\Modules\BaseAct as Base;

abstract class BaseAct extends Base
{
    /**
     * @return string
     * */
    public static function module(): string
    {
        return 'riccia';
    }
}
