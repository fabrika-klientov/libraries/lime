<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.14
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Riccia\Actions;

use Lime\Contracts\BeAction;
use Lime\Helpers\StateData;
use Lime\Modules\Riccia\Advanced\Controlling;
use Riccia\Models\Orders;

class GetOrderListAct extends BaseAct implements BeAction
{
    use Controlling, StateData;

    public function handle($inner, array $deep = [])
    {
        /**
         * @var \Lime\Modules\Riccia\Module $module
         * */
        $module = $this->modulesService->getModule($inner->module_uuid);
        if (empty($inner->inner) || empty($module)) {
            return;
        }

        /**
         * @var \Riccia\Client $client
         * */
        $client = $module->getClient();

        $instance = $client->orders;

        // query closure
        foreach ($inner->inner as $one) {
            [$alienModule, $alienValue] = $this->getMixedAlien($one, $deep);
            if (empty($alienValue)) {
                continue;
            }

            if (empty($one->values->topValue->prop->id)) {
                continue;
            }

            $instance->{$one->values->topValue->prop->id}($alienValue);
        }

        // get orders
        $result = $instance->get();

        if ($result->isNotEmpty()) {
            $this->addStateData($inner, $deep, 'orders', $result);

            $config = $module->getConfigs();
            $newLastTime = $result->reduce(
                function($result, Orders $order) {
                    $new = strtotime($order->created);
                    if (empty($result)) {
                        return $new;
                    }

                    return $new > $result ? $new : $result;
                },
                isset($config->last_fixed_date) ? strtotime($config->last_fixed_date) : null
            );

            $config->last_fixed_date = date('Y-m-d H:i:s', $newLastTime + 1);
            $config->save();
        }
    }

    public static function name(): string
    {
        return 'Получить список заказов';
    }

    public static function statically(): array
    {
        return [
            'deep' => [
                'type' => 'select',
                'extends' => 'deep|pull', // optionally (deep|pull...)
                'items' => [],
            ],
            'inner' => [
                'text' => 'По чему искать',
                'items' => [
                    ['id' => 'id', 'name' => 'ID заказа'],
                    ['id' => 'item_id', 'name' => 'ID товара в заказе | Название товара'],
                    ['id' => 'status', 'name' => 'Статус заказа'],
                    ['id' => 'delivery_id', 'name' => 'ID службы доставки'],
                    ['id' => 'created_from', 'name' => 'Дата создания заказа: от'],
                    ['id' => 'created_to', 'name' => 'Дата создания заказа: до'],
                    ['id' => 'changed_from', 'name' => 'Дата изменения от...'],
                    ['id' => 'changed_to', 'name' => 'Дата изменения до...'],
                    ['id' => 'userName', 'name' => 'ФИО покупателя'],
                    ['id' => 'user_phone', 'name' => 'Телефон покупателя'],
                    ['id' => 'seller_comment', 'name' => 'Комментарий продавца'],
                    ['id' => 'amountFrom', 'name' => 'Сумма заказа: от'],
                    ['id' => 'amountTo', 'name' => 'Сумма заказа: до'],
                    ['id' => 'type', 'name' => 'Типы заказов'],
                    ['id' => 'is_viewed', 'name' => 'Просмотрен ли заказ'],
                ],
            ],
        ];
    }

    public static function forDynamically(): ?string
    {
        return null;
    }

}
