<?php
/**
 * @package   Lime
 * @category  Black
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Black;

use Lantana\Models\JSBlackConfigs;
use Lantana\Models\JSBlackRelationFields;
use Lantana\Models\JSSharedCustomers;
use Lantana\Models\JSSharedCustomersServices;
use Lantana\Models\JSSharedModules;
use Lime\Contracts\BeConverting;
use Lime\Contracts\BeModule;
use Lime\Helpers\StateDataHelper;
use Lime\Modules\BaseModule;
use Lime\Modules\Black\Advanced\Converting;
use Lime\Services\DataStateService;

class Module extends BaseModule implements BeModule
{
    /**
     * @inheritDoc
     * */
    protected static $REGISTERED = [
        'conditions' => [],
        'actions' => [],
    ];
    /**
     * @var JSSharedModules $module
     * */
    private JSSharedModules $module;
    /**
     * @var JSBlackConfigs $blackConfigs
     * */
    private JSBlackConfigs $blackConfigs;
    /**
     * @var Converting $converting
     * */
    private $converting;

    public function __construct(JSSharedModules $module, JSBlackConfigs $blackConfigs)
    {
        $this->module = $module;
        $this->blackConfigs = $blackConfigs;
    }

    /**
     * @inheritDoc
     */
    public static function name(): string
    {
        return 'black';
    }

    /**
     * @inheritDoc
     */
    public static function description(): string
    {
        return 'Description for Black';
    }

    /**
     * @inheritDoc
     */
    public static function icon(): string
    {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function getPower(): bool
    {
        return $this->module->power;
    }

    /**
     * @inheritDoc
     */
    public function setPower(bool $power): void
    {
        $this->module->power = $power;
        $this->module->save();
    }

    /**
     * @inheritDoc
     */
    public function getID()
    {
        return $this->blackConfigs->name;
    }

    /**
     * @inheritDoc
     */
    public function getUUID()
    {
        return $this->module->uuid;
    }

    /**
     * @inheritDoc
     */
    public function getCustomerUUID()
    {
        return $this->module->customer_uuid;
    }

    /**
     * @inheritDoc
     */
    public function getClient()
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function fullDynamically(): array
    {
        $collect = $this->blackConfigs->blackRelationFields();

        return [
            'base' => array_merge(
                [
                    // your static data
                ],
                $collect
                    ->filter(fn($item) => isset($item->enterItem->code, $item->intermediaryItem->code))
                    ->map(fn($item) => ['id' => $item->uuid, 'name' => $item->intermediaryItem->title])
                    ->values()
                    ->toArray()
            ),
        ];
    }

    /**
     * @inheritDoc
     */
    protected function dynamically($condact = null): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getValueFor(DataStateService $dataStateService, $bottomValue, array $injectDeep)
    {
        if (!isset($bottomValue->prop->id)) {
            return null;
        }

        $relation = $this->blackConfigs
            ->blackRelationFields()
            ->first(fn(JSBlackRelationFields $item) => $item->uuid == $bottomValue->prop->id);
        if (empty($relation) || empty($relation->enterItem->keys)) {
            return null;
        }

        $state = new StateDataHelper($this, $dataStateService);
        $data = $state->getFromState(null, $injectDeep, 'free');
//        $data = $dataStateService->getForModule($this->getUUID(), 'free');
        if (empty($data)) {
            return null;
        }

        if (is_array($relation->enterItem->keys) && is_array($data)) {
            return $this->getValue($relation->enterItem->keys, $data);
        }

        return null;
    }

    public function getConverting(DataStateService $dataStateService): BeConverting
    {
        if (!isset($this->converting)) {
            $this->converting = new Converting();
        }

        return $this->converting;
    }

    /**
     * @param array $keys
     * @param array $data
     * @return mixed|null
     */
    protected function getValue(array $keys, array $data)
    {
        foreach ($keys as $key) {
            if (is_null($data)) {
                return null;
            }

            $data = $data[$key == '[]' ? 0 : $key] ?? null;
        }

        return $data;
    }

    /**
     * @inheritDoc
     */
    public static function init(JSSharedCustomers $customer)
    {
        $beforeCollect = JSSharedCustomersServices::where('shared-customers_uuid', $customer->uuid)
            ->where('code', 'black')
            ->get();

        if (empty($beforeCollect) || $beforeCollect->isEmpty()) {
            return collect();
        }

        $collect = JSBlackConfigs::wherein(
            'shared-customers-services_uuid',
            $beforeCollect->pluck('uuid')->values()->toArray()
        )
            ->withSharedModules()
            ->belongsToBlackRelationFields()
            ->get();

        if (empty($collect)) {
            return collect();
        }

        static::controlling($customer, $collect);

        return $collect->map(fn(JSBlackConfigs $item) => new self($item->{static::$table}, $item));
    }
}
