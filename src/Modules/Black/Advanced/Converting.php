<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.26
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Black\Advanced;

use Lime\Contracts\BeConverting;
use Lime\Contracts\BeExecuteConvert;

class Converting implements BeConverting
{
    /**
     * @inheritDoc
     * */
    public function getValue($bottomValue, array $deep)
    {
        return null;
    }

    /**
     * @inheritDoc
     * */
    public function getPreparingData($bottomValue, array $deep)
    {
        throw new \Exception('Not prepare');
    }

    /**
     * @inheritDoc
     * */
    public function getConvert(string $code): ?BeExecuteConvert
    {
        return null;
    }

}
