<?php
/**
 * @package   Lime
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules;

use Lime\Contracts\BeModule;

/**
 * @property-read string $uuid
 * @property-read string|mixed $id
 * @property-read string $name
 * @property-read string $description
 * @property-read string $icon
 * @property-read bool $power
 * //...
 * */
final class Entity implements \JsonSerializable
{
    /**
     * @var array $data
     * */
    private $data;
    /**
     * @var BeModule $module
     * */
    private $module;

    /**
     * @param BeModule $module
     * @return void
     * */
    public function __construct(BeModule $module)
    {
        $this->module = $module;
        $this->init();
    }

    /** init front (Serializable) data
     * @return void
     * */
    protected function init()
    {
        $this->data = [
            'uuid' => $this->module->getUUID(),
            'id' => $this->module->getID(),
            'name' => $this->module->name(),
            'description' => $this->module->description(),
            'icon' => $this->module->icon(),
            'power' => $this->module->getPower(),
            'customer_uuid' => $this->module->getCustomerUUID(),
        ];
    }

    /**
     * @param string $name
     * @return mixed
     * */
    public function __get($name)
    {
        return $this->data[$name] ?? null;
    }

    /**
     * @param string $name
     * @return bool
     * */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    /**
     * @return array
     * */
    public function jsonSerialize()
    {
        return $this->data;
    }

    /**
     * @return string
     * */
    public function __toString()
    {
        return (string)json_encode($this->data);
    }
}
