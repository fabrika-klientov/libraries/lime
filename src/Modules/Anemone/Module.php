<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.01
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone;

use Anemone\Client;
use Anemone\Contracts\BeModel;
use Anemone\Models\Tag;
use Illuminate\Support\Collection;
use Lantana\Models\JSSharedAmocrm;
use Lantana\Models\JSSharedCustomers;
use Lantana\Models\JSSharedModules;
use Lime\Contracts\BeConverting;
use Lime\Contracts\BeModule;
use Lime\Helpers\StateDataHelper;
use Lime\Modules\Anemone\Advanced\Converting;
use Lime\Modules\Anemone\Advanced\Helpers\SchemeData;
use Lime\Modules\BaseModule;
use Lime\Services\DataStateService;

class Module extends BaseModule implements BeModule
{
    use SchemeData;

    /**
     * @inheritDoc
     * */
    protected static $REGISTERED = [
        'conditions' => [
            \Lime\Modules\Anemone\Conditions\ContactFoundCond::class,
            \Lime\Modules\Anemone\Conditions\CompanyFoundCond::class,
            \Lime\Modules\Anemone\Conditions\LeadFoundCond::class,
            \Lime\Modules\Anemone\Conditions\TaskFoundCond::class,
            \Lime\Modules\Anemone\Conditions\NoteFoundCond::class,
        ],
        'actions' => [
            \Lime\Modules\Anemone\Actions\FindContactAct::class,
            \Lime\Modules\Anemone\Actions\FindCompanyAct::class,
            \Lime\Modules\Anemone\Actions\FindLeadAct::class,
            \Lime\Modules\Anemone\Actions\FindTaskAct::class,
            \Lime\Modules\Anemone\Actions\FindNoteAct::class,
            \Lime\Modules\Anemone\Actions\StoreContactAct::class,
            \Lime\Modules\Anemone\Actions\StoreCompanyAct::class,
            \Lime\Modules\Anemone\Actions\StoreLeadAct::class,
            \Lime\Modules\Anemone\Actions\StoreTaskAct::class,
            \Lime\Modules\Anemone\Actions\StoreNoteAct::class,
            \Lime\Modules\Anemone\Actions\StoreCallAct::class,
            \Lime\Modules\Anemone\Actions\UpdateFirstContactAct::class,
            \Lime\Modules\Anemone\Actions\UpdateFirstCompanyAct::class,
            \Lime\Modules\Anemone\Actions\UpdateFirstLeadAct::class,
            \Lime\Modules\Anemone\Actions\UpdateFirstTaskAct::class,
            \Lime\Modules\Anemone\Actions\UpdateFirstNoteAct::class,
            \Lime\Modules\Anemone\Actions\LinkEntitiesAct::class,
        ],
    ];
    /**
     * @var JSSharedModules $module
     * */
    private JSSharedModules $module;
    /**
     * @var JSSharedAmocrm $amocrm
     * */
    private JSSharedAmocrm $amocrm;
    /**
     * @var Converting $converting
     * */
    private $converting;

    public function __construct(JSSharedModules $module, JSSharedAmocrm $amocrm)
    {
        $this->module = $module;
        $this->amocrm = $amocrm;
    }

    /**
     * @inheritDoc
     */
    public static function name(): string
    {
        return 'anemone';
    }

    /**
     * @inheritDoc
     */
    public static function description(): string
    {
        return 'Lorem ipsum dolor sit amet, consectetur 
        adipiscing elit, sed do eiusmod tempor incididunt ut labore et 
        dolore magna aliqua.';
    }

    /**
     * @inheritDoc
     */
    public static function icon(): string
    {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function getPower(): bool
    {
        return $this->module->power;
    }

    /**
     * @inheritDoc
     */
    public function setPower(bool $power): void
    {
        $this->module->power = $power;
        $this->module->save();
    }

    /**
     * @inheritDoc
     */
    public function getID()
    {
        return $this->amocrm->domain;
    }

    /**
     * @inheritDoc
     */
    public function getUUID()
    {
        return $this->module->uuid;
    }

    /**
     * @inheritDoc
     */
    public function getCustomerUUID()
    {
        return $this->module->customer_uuid;
    }

    /**
     * @inheritDoc
     * @return Client
     */
    public function getClient()
    {
        return new Client($this->amocrm->toArray());
    }

    /**
     * @inheritDoc
     * @param \Lime\Contracts\BeCondact|null $condact
     * */
    protected function dynamically($condact = null): array
    {
        if (empty($condact)) {
            return self::getCF();
        }

        if (!class_exists($condact)) {
            return [];
        }

        $key = $condact::forDynamically();
        switch ($key) {
            case 'lead':
            case 'contact':
            case 'company':
            case 'customer':
            case 'task':
            case 'note':
            case 'call':
                $collect = self::getCF($key);
                return isset($collect) ? $collect->values()->toArray() : [];
        }

        return [];
    }

    /**
     * @inheritDoc
     * */
    public function fullDynamically(): array
    {
        $data = $this->dynamically();
        foreach ($data as &$datum) {
            $datum = $datum->values();
        }

        return $data;
    }

    /**
     * @return bool
     * */
    protected function isCustomers(): bool
    {
        return !in_array($this->getClient()->account->get()->customers_mode, ['disabled', 'unavailable']);
    }

    /**
     * @inheritDoc
     * */
    public function getValueFor(DataStateService $dataStateService, $bottomValue, array $injectDeep)
    {
        if (!isset($bottomValue->prop->top_code, $bottomValue->prop->id)) {
            return null;
        }

        $state = new StateDataHelper($this, $dataStateService);
        $switch = $bottomValue->prop->id;

        // for converting
        try {
            [$switch] = $this
                ->getConverting($dataStateService)
                ->getPreparingData($bottomValue, $injectDeep);
        } catch (\Exception $e) {
        }

        switch ($switch) {
            case 'system_collection':
                $data = $state->getFromState(null, $injectDeep, self::convertCode($bottomValue->prop->top_code));
                return $data ?? null;

            case 'item_iterator':
                if (count($injectDeep) < 2) {
                    $data = $state->getFromState(null, $injectDeep, self::convertCode($bottomValue->prop->top_code));
                }
                if (empty($data)) {
                    $data = collect([last($injectDeep)]);
                }
                break;

            case 'anemone_pipeline':
                return $bottomValue->prop->value->pipeline_id ?? null;

            case 'anemone_pipeline_status':
                return $bottomValue->prop->value->status_id ?? null;

            default:
                // converted data
                if (strpos($bottomValue->prop->id, 'anemone') === 0) {
                    return $this->getConverting($dataStateService)->getValue($bottomValue, $injectDeep);
                }

                if ($switch == 'tags' && !empty($bottomValue->prop->value)) { // get selected tags of front tag select
                    return $bottomValue->prop->value;
                }

                $data = $state->getFromState(null, $injectDeep, self::convertCode($bottomValue->prop->top_code));
        }

        if ($data instanceof Collection) {
            if ($data->isEmpty()) {
                return null;
            }

            $injectEntity = $this->getDeepEntity($injectDeep, $bottomValue);
            if (is_object($injectEntity) &&
                strpos(get_class($injectEntity), 'Anemone') !== false &&
                $data->some('id', $injectEntity->id)
            ) {
                $data = $injectEntity;
            } else {
                $data = $data->first();
            }

            if (preg_match('/^(entities|collection)/i', $bottomValue->prop->id)) { // get model for converting
                return $data;
            }
        }

        if (!$data instanceof BeModel) {
            return null;
        }

        $key = $bottomValue->prop->id == 'item_iterator' ? $bottomValue->prop->value->id : $bottomValue->prop->id;
        $cf = $data->cf($key); // for CF
        if (isset($cf)) {
            return $cf->getValue();
        }

        if ($key == 'tags' && method_exists($data, $key)) { // get tags of model
            return $data->tags()->map(fn(Tag $tag) => $tag->name)->values()->toArray();
        }

        return $data->{$key} ?? null;
    }

    /**
     * @inheritDoc
     * */
    public function getConverting(DataStateService $dataStateService): BeConverting
    {
        if (!isset($this->converting)) {
            $this->converting = new Converting($dataStateService, $this);
        }

        return $this->converting;
    }


    /**
     * @param string $code
     * @return string
     */
    protected static function convertCode(string $code): string
    {
        switch ($code) {
            case 'lead':
            case 'contact':
            case 'customer':
            case 'task':
            case 'note':
            case 'call':
                return $code . 's';
            case 'company':
                return 'companies';
        }
        return $code;
    }

    /**
     * @inheritDoc
     *
     * @param JSSharedCustomers $customer
     * @return \Illuminate\Support\Collection
     */
    public static function init(JSSharedCustomers $customer)
    {
        $collect = JSSharedAmocrm::where('shared-customers_uuid', $customer->uuid)
            ->withSharedModules()
            ->get();

        if (empty($collect)) {
            return collect();
        }

        static::controlling($customer, $collect);

        return $collect->map(fn(JSSharedAmocrm $item) => new self($item->{static::$table}, $item));
    }
}
