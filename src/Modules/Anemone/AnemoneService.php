<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone;

use Lime\Contracts\BeAgent;
use Lime\Contracts\BeModule;
use Lime\Contracts\BeService;
use Lime\Exceptions\LimeException;
use Lime\Modules\Anemone\Inject\HookDecorator;
use Lime\Services\ModulesService;

class AnemoneService implements BeAgent, BeService
{
    /**
     * @var ModulesService $modulesService
     * */
    private $modulesService;

    /**
     * @param ModulesService $modulesService
     * @return void
     * @throws LimeException
     */
    public function __construct(ModulesService $modulesService)
    {
        if (!$modulesService->isReady()) {
            throw new LimeException('Service not ready');
        }
        $this->modulesService = $modulesService;
    }

    /**
     * @param mixed $data
     *
     */
    public function up($data = null)
    {
    }

    /**
     * @param array $data
     *
     * @return HookDecorator
     * @throws LimeException
     * @throws \Exception
     */
    public function hook(array $data)
    {
        $subdomain = $this->getSubdomain($data);
        if (empty($subdomain)) {
            throw new LimeException('Invalid amo hook data');
        }
        $subdomain .= '.';

        /**
         * @var \Lime\Modules\Anemone\Module|null $module
         * */
        $module = $this->modulesService
            ->getModules()
            ->first(fn(BeModule $module) => Module::name() == 'anemone' && strpos($module->getID(), $subdomain) === 0);
        if (empty($module)) {
            throw new LimeException('For ' . $subdomain . '.. Module not found');
        }

        return new HookDecorator($module, $data);
    }

    /**
     * @param array $data
     * @return string|null
     * */
    protected function getSubdomain(array $data)
    {
        return $data['account']['subdomain'] ?? null;
    }
}
