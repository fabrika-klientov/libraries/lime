<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.04
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Advanced;

use Lantana\Models\JSLimeConvert;
use Lime\Contracts\BeConverting;
use Lime\Contracts\BeExecuteConvert;
use Lime\Modules\Anemone\Advanced\Convert\NextResponsible;
use Lime\Modules\Anemone\Module;
use Lime\Services\DataStateService;

class Converting implements BeConverting
{
    private DataStateService $dataStateService;
    private Module $module;

    /**
     * @param DataStateService $dataStateService
     * @param Module $module
     */
    public function __construct(DataStateService $dataStateService, Module $module)
    {
        $this->dataStateService = $dataStateService;
        $this->module = $module;
    }

    /**
     * @inheritDoc
     */
    public function getValue($bottomValue, array $deep)
    {
        switch ($bottomValue->prop->id) {
            case 'anemone_next_responsible':
                if (empty($bottomValue->prop->value)) {
                    return null;
                }

                $convertRule = JSLimeConvert::where('type', $bottomValue->prop->id)
                    ->find($bottomValue->prop->value);
                if (empty($convertRule) || empty($convertRule->deep->{'key'})) {
                    return null;
                }

                return $this->getConvert($bottomValue->prop->id)->handle($convertRule, $this->module->getClient());
        }

        return null;
    }

    /**
     * @inheritDoc
     * */
    public function getPreparingData($bottomValue, array $deep)
    {
        if (strpos($bottomValue->prop->id, 'entities') === 0) {
            $switch = 'item_iterator';

            return [$switch];
        } elseif (strpos($bottomValue->prop->id, 'collection') === 0) {
            $switch = 'system_collection';

            return [$switch];
        }

        throw new \Exception('Not prepare');
    }

    /**
     * @inheritDoc
     * */
    public function getConvert(string $code): ?BeExecuteConvert
    {
        switch ($code) {
            case 'anemone_next_responsible':
                return new NextResponsible();
        }

        return null;
    }

}
