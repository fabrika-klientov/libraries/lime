<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Advanced;

use Illuminate\Support\Str;

trait Filtering
{

    /**
     * @param mixed $one
     * @param mixed $alienValue
     * @return \Closure
     */
    protected function closureCF($one, $alienValue)
    {
        return function ($entity) use ($one, $alienValue): bool {
            $cf = $entity->cf($one->values->topValue->prop->id);
            if (empty($cf)) {
                return false;
            }

            return collect($cf->values)->some(fn($one) => Str::upper($one['value']) == Str::upper((string)$alienValue));
        };
    }

    /**
     * @param mixed $one
     * @param mixed $alienValue
     * @return \Closure
     */
    protected function closureBaseField($one, $alienValue)
    {
        return function ($entity) use ($one, $alienValue): bool {
            switch ($one->values->topValue->prop->id) {
                case 'name': // name of entities
                    $field = $entity->name;
                    break;

                case 'price': // for lead
                case 'next_price': // for customer
                case 'ltv': // for customer
                case 'text': // for task
                case 'task_type_id': // for task
                case 'complete_till': // for task
                case 'entity_id': // for task
                case 'entity_type': // for task
                case 'is_completed': // for task
                    $field = $entity->{$one->values->topValue->prop->id};
                    break;

                case 'pipeline': // for lead
                case 'status': // for lead
                    $field = $entity->{$one->values->topValue->prop->id . '_id'};
                    break;

                case 'responsible':
                    $field = $entity->responsible_user_id;
                    break;

                case 'result_text': // for task
                    $field = $entity->result['text'] ?? null;
                    break;

                default:
                    $field = null;
            }

            return isset($field) ? Str::upper($field) == Str::upper((string)$alienValue) : false;
        };
    }
}
