<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Advanced;

use Illuminate\Support\Str;

trait SetCF
{
    /**
     * @param mixed $entity
     * @param mixed $one
     * @param mixed $alienValue
     * @return void
     */
    protected function setCF($entity, $one, $alienValue)
    {
        $cf = $entity->cf($one->values->topValue->prop->id);
        if (empty($cf)) {
            return;
        }

        $one->values->topValue->replace ??= 'update';
        switch ($one->values->topValue->replace) { // expected enum in: add, unique, update, delete
            case 'add':
                if (Str::upper($cf->code) == 'PHONE' || Str::upper($cf->code) == 'EMAIL') {
                    $cf->setValue((string)$alienValue);
                    return;
                }
                break;

            case 'unique':
                if (Str::upper($cf->code) == 'PHONE' || Str::upper($cf->code) == 'EMAIL') {
                    $values = $cf->getValue('WORK', false);
                    if (in_array((string)$alienValue, $values ?? [], true)) {
                        return;
                    }

                    $cf->setValue((string)$alienValue);
                    return;
                }
                break;

            case 'update':
                if (Str::upper($cf->code) == 'PHONE' || Str::upper($cf->code) == 'EMAIL') {
                    $cf->setValue((string)$alienValue, 'WORK', true);
                    return;
                }
                break;

            case 'delete':
                break;
        }

        $cf->setValue($alienValue);
    }
}
