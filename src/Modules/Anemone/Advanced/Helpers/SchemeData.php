<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.15
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Advanced\Helpers;

use Anemone\Core\Collection\Collection;

trait SchemeData
{
    /**
     * @var array $baseEntities
     * */
    protected static array $baseEntities = [
        'leads',
        'customers',
        'contacts',
        'companies',
        'tasks',
        'notes',
        'calls',
    ];

    /**
     * @param string|null $key
     * @return Collection|array
     */
    protected function getCF(string $key = null)
    {
        try {
            if (isset($key) && in_array(static::convertCode($key), static::$baseEntities)) {
                return $this->helperLoadForEntity(static::convertCode($key));
            }

            return [
                'lead' => $this->helperLoadForEntity('leads'),
                'customer' => $this->helperLoadForEntity('customers'),
                'contact' => $this->helperLoadForEntity('contacts'),
                'company' => $this->helperLoadForEntity('companies'),
                'task' => $this->helperLoadForEntity('tasks'),
                'note' => $this->helperLoadForEntity('notes'),
                'call' => $this->helperLoadForEntity('calls'),
            ];
        } catch (\Exception $exception) {
            // not loaded data for this module
        }


        return isset($key) ? new Collection() : [];
    }

    /**
     * @param string|null $key
     * @return Collection|array
     */
    protected function getTags(string $key = null)
    {
        try {
            $collectHP = fn($entity) => $entity == 'customers' && !$this->isCustomers()
                ? new Collection()
                : $this->getClient()->{$entity}->tags()->get();

            if (isset($key) && in_array(static::convertCode($key), static::$baseEntities)) {
                return $collectHP(static::convertCode($key));
            }

            return [
                'lead' => $collectHP('leads'),
                'customer' => $collectHP('customers'),
                'contact' => $collectHP('contacts'),
                'company' => $collectHP('companies'),
            ];
        } catch (\Exception $exception) {
            // not loaded data for this module
        }

        return [];
    }

    /**
     * @param string $entity
     * @return Collection
     */
    protected function helperLoadForEntity(string $entity)
    {
        if ($entity == 'customers' && !$this->isCustomers()) {
            return new Collection();
        }

        $instance = $this->getClient()->{$entity};

        $collect = new Collection(
            collect(static::topProps($entity))
                ->map(
                    function ($one) use ($entity) {
                        if ($one['id'] == 'tags') {
                            $one['additional'] = static::getTags($entity)->values();
                        }
                        return $one;
                    }
                )
        );

        // load cf
        if (method_exists($instance, 'cf')) {
            $collect = $collect->merge($instance->cf());
        }

        return $collect;
    }

    /**
     * @param string|null $code
     * @return array
     */
    public static function topProps(string $code = null)
    {
        $props = [
            'leads' => [
                ['id' => 'name', 'name' => 'Название сделки'],
                ['id' => 'responsible', 'name' => 'Ответственный сделки'],
                ['id' => 'sale', 'name' => 'Бюджет'],
                ['id' => 'pipeline', 'name' => 'Воронка'],
                ['id' => 'status', 'name' => 'Этап воронки'],
                ['id' => 'tags', 'name' => 'Теги'],
            ],
            'customers' => [
                ['id' => 'name', 'name' => 'Название покупателя'],
                ['id' => 'responsible', 'name' => 'Ответственный покупателя'],
                ['id' => 'next_price', 'name' => 'Сумма покупок'],
                ['id' => 'tags', 'name' => 'Теги'],
            ],
            'contacts' => [
                ['id' => 'name', 'name' => 'Название контакта'],
                ['id' => 'responsible', 'name' => 'Ответственный контакта'],
                ['id' => 'tags', 'name' => 'Теги'],
            ],
            'companies' => [
                ['id' => 'name', 'name' => 'Название компании'],
                ['id' => 'responsible', 'name' => 'Ответственный компании'],
                ['id' => 'tags', 'name' => 'Теги'],
            ],
            'tasks' => [
                ['id' => 'text', 'name' => 'Описание задачи'],
                ['id' => 'responsible', 'name' => 'Ответственный за задачу'],
                ['id' => 'task_type_id', 'name' => 'Тип задачи'],
                ['id' => 'complete_till', 'name' => 'Дата завершения задачи'],
                ['id' => 'entity_id', 'name' => 'ID сущности, к которой привязана задача'],
                ['id' => 'entity_type', 'name' => 'Тип сущности, к которой привязана задача'],
                ['id' => 'is_completed', 'name' => 'Выполнена ли задача'],
                ['id' => 'result_text', 'name' => 'Текст результата выполнения задачи'],
            ],
            'notes' => [
                ['id' => 'note_type', 'name' => 'Тип примечания'],
                ['id' => 'entity_type', 'name' => 'Тип сущности'],
                ['id' => 'entity_id', 'name' => 'ID сущности'],
                [
                    'id' => 'params_text',
                    'name' => 'Текст примечания (common, service_message, message_cashier, invoice_paid, geolocation, sms_in, sms_out)',
                ],
                ['id' => 'params_uniq', 'name' => 'uniq примечания (call_in, call_out)'],
                ['id' => 'params_duration', 'name' => 'duration примечания (call_in, call_out)'],
                ['id' => 'params_source', 'name' => 'source примечания (call_in, call_out)'],
                ['id' => 'params_link', 'name' => 'link примечания (call_in, call_out)'],
                ['id' => 'params_phone', 'name' => 'phone примечания (call_in, call_out, sms_in, sms_out)'],
                ['id' => 'params_service', 'name' => 'service примечания (service_message, invoice_paid)'],
                ['id' => 'params_status', 'name' => 'status примечания (message_cashier)'],
                ['id' => 'params_icon_url', 'name' => 'icon_url примечания (invoice_paid)'],
                ['id' => 'params_address', 'name' => 'address примечания (geolocation)'],
                ['id' => 'params_longitude', 'name' => 'longitude примечания (geolocation)'],
                ['id' => 'params_latitude', 'name' => 'latitude примечания (geolocation)'],
            ],
            'calls' => [
                ['id' => 'direction', 'name' => 'Направление звонка'],
                ['id' => 'uniq', 'name' => 'Уникальный идентификатор звонка'],
                ['id' => 'duration', 'name' => 'Длительность звонка в секундах'],
                ['id' => 'source', 'name' => 'Источник звонка'],
                ['id' => 'link', 'name' => 'Ссылка на запись звонка'],
                ['id' => 'phone', 'name' => 'Номер телефона'],
                ['id' => 'call_result', 'name' => 'Результат звонка'],
                ['id' => 'call_result', 'name' => 'Результат звонка'],
                ['id' => 'call_status', 'name' => 'Статус звонка'],
            ],
        ];

        if (isset($code, $props[static::convertCode($code)])) {
            return $props[static::convertCode($code)];
        }

        return $props;
    }
}
