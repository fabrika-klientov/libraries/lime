<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Advanced;

use Anemone\Models\Company;
use Anemone\Models\Contact;
use Anemone\Models\Customer;
use Anemone\Models\Lead;
use Anemone\Models\Task;

trait SetBase
{
    /**
     * @param mixed $entity
     * @param mixed $one
     * @param mixed $alienValue
     * @return void
     */
    protected function setBase($entity, $one, $alienValue)
    {
        $one->values->topValue->replace ??= 'update';

        switch ($one->values->topValue->prop->id) {
            case 'name': // name of entities
                $entity->name = (string)$alienValue;
                break;

            case 'price': // for lead
            case 'next_price': // for customer
            case 'ltv': // for customer
            case 'text': // for task
            case 'task_type_id': // for task
            case 'complete_till': // for task
            case 'entity_id': // for task
            case 'entity_type': // for task
            case 'is_completed': // for task
                $entity->{$one->values->topValue->prop->id} = (string)$alienValue;
                break;

            case 'pipeline': // for lead
            case 'status': // for lead
                $entity->{$one->values->topValue->prop->id . '_id'} = (string)$alienValue;
                break;

            case 'responsible':
                $entity->responsible_user_id = (string)$alienValue;
                break;

            case 'tags':
                if (!method_exists($entity, 'tags')) {
                    return;
                }

                switch ($one->values->topValue->replace) {
                    case 'add':
                        $entity->attachTags($alienValue);
                        break;

                    case 'update':
                    case 'unique':
                        $entity->detachTags($entity->tags());
                        $entity->attachTags($alienValue);
                        break;

                    case 'delete':
                        $entity->detachTags($alienValue);
                        break;
                }
                break;

            case 'result_text': // for task
                $entity->result = ['text' => (string)$alienValue];
                break;

            case 'entity':
                if (!is_object($alienValue)) {
                    return;
                }

                if ($entity instanceof Task) {
                    switch (get_class($alienValue)) {
                        case Lead::class:
                            $entity->entity_type = 'leads';
                            $entity->entity_id = $alienValue->id;
                            break;
                        case Contact::class:
                            $entity->entity_type = 'contacts';
                            $entity->entity_id = $alienValue->id;
                            break;
                        case Company::class:
                            $entity->entity_type = 'companies';
                            $entity->entity_id = $alienValue->id;
                            break;
                        case Customer::class:
                            $entity->entity_type = 'customers';
                            $entity->entity_id = $alienValue->id;
                            break;
                    }
                } // other if need
                break;
        }
    }
}
