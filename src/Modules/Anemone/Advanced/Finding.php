<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Advanced;

trait Finding
{
    use Filtering;

    /**
     * @param mixed $instance
     * @param mixed $inner
     * @param array $inject
     * @return \Illuminate\Support\Collection
     */
    protected function findEntities($instance, $inner, array $inject)
    {
        $result = collect();

        foreach ($inner->inner as $one) {
            [$alienModule, $alienValue] = $this->getMixedAlien($one, $inject);
            if (empty($alienValue)) {
                continue;
            }

            if (empty($one->values->topValue->prop->id)) {
                continue;
            }

            // helper closure filtering for CF, FIELD, ...
            if (is_numeric($one->values->topValue->prop->id)) {
                $closure = $this->closureCF($one, $alienValue);
            } else {
                $closure = $this->closureBaseField($one, $alienValue);
            }

            // operator actions
            /**
             * @var mixed $one
             * */
            if (isset($one->operator->type)) {
                switch ($one->operator->type) {
                    case 'and':
                        if ($result->isNotEmpty()) {
                            $result = $result->filter($closure);
                            if ($result->isNotEmpty()) {
                                continue 2;
                            }
                        }
                        break 2;
                    case 'or':
                        if ($result->isNotEmpty()) {
                            break 2;
                        }
                        break;
                }
            }

            // !! upd in future
            if (method_exists($instance, 'query')) { // is deprecated
                $result = $result->merge($instance->query((string)$alienValue)->get()->filter($closure));
            } elseif (method_exists($instance, 'filter')) { // is new version (alpha)
                $result = $result->merge(
                    $instance->filter($one->values->topValue->prop->id, $alienValue)->get()->filter($closure)
                );
            }
        }

        return $result;
    }

    /** in future
     * @param string $code
     * @param $value
     */
    protected function adaptingFilter(string $code, $value)
    {

    }
}
