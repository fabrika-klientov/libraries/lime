<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.04
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Advanced\Convert;

use Anemone\Extensions\Distribution\Distribution;
use Anemone\Extensions\Distribution\DistributionService;
use Lantana\Models\JSLimeConvert;
use Lantana\Models\JSSharedApikeys;
use Lantana\Models\JSSharedDistributionConfigs;
use Lime\Contracts\BeExecuteConvert;

class NextResponsible implements BeExecuteConvert
{

    public function __construct()
    {
    }

    /**
     * @inheritDoc
     * @param \Anemone\Client $client
     * */
    public function handle(JSLimeConvert $limeConvert, $client = null)
    {
        $apiKey = JSSharedApikeys::where('key', $limeConvert->deep->{'key'})
            ->belongsToSharedCustomersServices()
            ->first();
        if (empty($apiKey) ||
            empty($apiKey->sharedCustomersServices) ||
            $apiKey->sharedCustomersServices->isEmpty()
        ) {
            return null;
        }

        /**
         * @var JSSharedDistributionConfigs|null $sharedDistributionConfigs
         * */
        $sharedDistributionConfigs = JSSharedDistributionConfigs::where(
            'shared-customers-services_uuid',
            $apiKey->sharedCustomersServices->first()->uuid)
            ->first();

        if ($sharedDistributionConfigs) {
            $distributionService = new DistributionService($client);
            $distributionService->init(new Distribution($sharedDistributionConfigs->toArray()));
            $next = $distributionService->next();

            (new JSSharedDistributionConfigs($distributionService->result()))->save();

            return $next;
        }

        return null;
    }
}
