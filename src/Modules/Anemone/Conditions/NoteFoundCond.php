<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.15
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Conditions;

use Lime\Contracts\BeCondition;
use Lime\Helpers\StateData;

class NoteFoundCond extends BaseCond implements BeCondition
{
    use StateData;

    public function handle($inner = null, array $deep = []): bool
    {
        $module = $this->modulesService->getModule($inner->module_uuid);
        if (empty($inner->deep) || empty($module)) {
            return false;
        }

        $collect = $this->getFromState($inner, $deep, 'notes');
        $operator = $inner->deep->items[0]->type ?? null;
        if (empty($operator)) {
            return false;
        }

        switch ($operator) {
            case 'yes':
                return isset($collect) ? $collect->isNotEmpty() : false;
            case 'no':
                return isset($collect) ? $collect->isEmpty() : true;
        }

        return false;
    }

    public static function name(): string
    {
        return 'Примечание найдено';
    }

    /**
     * extends base return [type, module, name, ...]
     * @return array
     * */
    public static function statically(): array
    {
        return [
            'deep' => [
                'type' => 'select',
                'items' => [
                    [
                        'type' => 'yes',
                        'name' => 'Да',
                    ],
                    [
                        'type' => 'no',
                        'name' => 'Нет',
                    ],
//                    [ // in other conditions
//                        'type' => 'equals',
//                        'name' => '=',
//                        'deep' => [
//                            'type' => 'input',
//                        ]
//                    ],
//                    [
//                        'type' => 'not_equals',
//                        'name' => '!=',
//                        'deep' => [
//                            'type' => 'input',
//                        ]
//                    ],
                ],
            ],
            'operator' => [
                'type' => 'and',
                'name' => 'И',
                'blocked' => true,
            ],
        ];
    }

    public static function forDynamically(): ?string
    {
        return null;
    }
}
