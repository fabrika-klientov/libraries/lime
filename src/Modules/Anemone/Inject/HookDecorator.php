<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Inject;

use Lime\Contracts\BeInject;
use Lime\Exceptions\LimeException;
use Lime\Modules\Anemone\Module;
use Lime\Services\DataStateService;

class HookDecorator implements BeInject
{
    /**
     * @var Module $module
     * */
    private $module;
    /**
     * @var array $free
     * */
    private $free;
    /**
     * @var array $available
     * */
    protected static $available = [
        'leads' => ['add', 'update', 'status', 'responsible', /**...*/],
        'contacts' => ['add', 'update', 'responsible', /**...*/],
        'companies' => ['add', 'update', 'responsible', /**...*/],
        'customers' => ['add', 'update', 'responsible', /**...*/],
    ];
    /**
     * @var string $activeEntity
     * */
    protected $activeEntity;
    /**
     * @var string $activeAction
     * */
    protected $activeAction;
    /**
     * @var \Anemone\Core\Collection\Collection|null
     * */
    protected $collection;

    /**
     * @param Module $module
     * @param array $free
     * @throws LimeException
     * @throws \Anemone\Exceptions\InvalidDataException
     */
    public function __construct(Module $module, array $free)
    {
        $this->module = $module;
        $this->free = $free;
    }

    /**
     *
     * @param DataStateService $dataStateService
     * @throws LimeException
     * @throws \Anemone\Exceptions\InvalidDataException
     */
    public function up(DataStateService $dataStateService)
    {
        foreach (self::$available as $entity => $actions) {
            if (empty($this->free[$entity]) || !is_array($this->free[$entity])) {
                continue;
            }

            $this->activeEntity = $entity;
            foreach ($actions as $action) {
                if (empty($this->free[$entity][$action]) || !is_array($this->free[$entity][$action])) {
                    continue;
                }

                $this->activeAction = $action;
                $instance = $this->getAnemoneInstance();
                if (empty($instance)) {
                    throw new LimeException('For ' . $this->activeEntity . ' instance not found');
                }

                $service = $instance->getServiceAdapter();
                $service->fromCache(true);
                $this->collection = $service->getData($this->free[$entity][$action]);

                // inject in DataStateService
                $dataStateService->setForModule($this->module->getUUID(), $this->activeEntity, $this->collection);
                return;
            }
        }

        throw new LimeException('For amo hook data not found associated adapters and decorators');
    }

    /**
     * @param DataStateService $dataStateService
     */
    public function down(DataStateService $dataStateService)
    {
        $this->collection = null;
    }

    /**
     * @return \Anemone\Core\Collection\Collection|null
     * */
    public function getData()
    {
        return $this->collection;
    }

    /**
     * @return string|null
     * */
    public function getHookEntity()
    {
        return $this->activeEntity;
    }

    /**
     * @return string|null
     * */
    public function getHookAction()
    {
        return $this->activeAction;
    }

    /**
     * @return array
     * */
    public function getFreeData()
    {
        return $this->free;
    }

    /**
     * @return string
     * */
    public function getDomain()
    {
        return $this->module->getClient()->getDomain();
    }

    /**
     * @return \Anemone\Models\Instances\ModelInstance|null
     * */
    protected function getAnemoneInstance()
    {
        switch ($this->activeEntity) {
            case 'leads':
            case 'contacts':
            case 'companies':
            case 'customers':
                return $this->module->getClient()->{$this->activeEntity};
        }

        return null;
    }
}
