<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.15
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Actions;

use Anemone\Core\Collection\Collection;
use Anemone\Models\Note;
use Illuminate\Support\Str;
use Lime\Contracts\BeAction;
use Lime\Helpers\StateData;
use Lime\Modules\Anemone\Advanced\Controlling;

class UpdateFirstNoteAct extends BaseAct implements BeAction
{
    use Controlling, StateData;

    public function handle($inner, array $deep = [])
    {
        $module = $this->modulesService->getModule($inner->module_uuid);
        if (empty($inner->inner) || empty($module)) {
            return;
        }

        $last = last($deep);
        $collect = $this->getFromState($inner, $deep, 'notes');
        $first = isset($collect) ? $collect->first() : ($last instanceof Note ? $last : null);

        if (empty($first)) {
            return;
        }

        foreach ($inner->inner as $one) {
            [$alienModule, $alienValue] = $this->getMixedAlien($one, $deep);
            if (empty($alienValue)) {
                continue;
            }

            if (!isset($one->values->topValue->prop->id)) {
                continue;
            }

            $code = $one->values->topValue->prop->id;
            if (strpos($one->values->topValue->prop->id, 'params') === 0) {
                $code = Str::after($one->values->topValue->prop->id, 'params_');
            }

            switch ($code) {
                case 'note_type':
                case 'entity_id':
                    $first->{$code} = $alienValue;
                    break;

                case 'text':
                case 'uniq':
                case 'duration':
                case 'source':
                case 'link':
                case 'phone':
                case 'service':
                case 'status':
                case 'icon_url':
                case 'address':
                case 'longitude':
                case 'latitude':
                    $first->params = array_merge($first->params, [$code => $alienValue]);
                    break;
            }
        }

        if (empty($first->entity_type)) {
            return;
        }

        $tmpInstance = $module->getClient()->{$first->entity_type};
        if (empty($tmpInstance) || !method_exists($tmpInstance, 'notes')) {
            return;
        }

        /**
         * @var \Anemone\Models\Instances\NotesInstance $instance
         * */
        $instance = $tmpInstance->notes();

        $instance->save(new Collection([$first]));
    }

    public static function name(): string
    {
        return 'Обновить первое найденное примечание';
    }

    public static function statically(): array
    {
        return [
            'deep' => [
                'type' => 'select',
                'extends' => 'deep|pull', // optionally (deep|pull...)
                'items' => [],
            ],
            'inner' => [
                'extends' => 'dynamically', // optional extends items
                'text' => 'Параметр',
                'items' => [], // for merging
            ],
            'replace' => false,
        ];
    }

    public static function forDynamically(): ?string
    {
        return 'note';
    }
}
