<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.22
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Actions;

use Anemone\Core\Collection\Collection;
use Anemone\Models\Company;
use Anemone\Models\Contact;
use Anemone\Models\Customer;
use Anemone\Models\Lead;
use Lime\Contracts\BeAction;
use Lime\Helpers\StateData;

class LinkEntitiesAct extends BaseAct implements BeAction
{
    use StateData;

    public function handle($inner, array $deep = [])
    {
        $module = $this->modulesService->getModule($inner->module_uuid);
        if (empty($inner->inner) || empty($module)) {
            return;
        }

        if (empty($inner->inner) || count($inner->inner) < 2) { // 2 and more entities
            return;
        }

        $data = [];
        foreach ($inner->inner as $item) {
            if (empty($item->values->topValue->prop->id)) {
                continue;
            }
            $data[$item->values->topValue->prop->id] = $this->getFromState(
                (object)['module_uuid' => $inner->module_uuid, 'deep' => $item->values->topValue->deep ?? null],
                $deep,
                $item->values->topValue->prop->id
            );
        }

        $data = array_filter($data, fn($one) => isset($one) && $one->isNotEmpty());

        if (count($data) < 2) { // 2 and more entities
            return;
        }

        /**
         * @var \Anemone\Client $client
         * */
        $client = $module->getClient();

        $baseCollect = $data['leads'] ?? $data['customers'] ?? $data['companies'];
        if (isset($baseCollect)) {
            $strInstance = null;
            $baseCollect->each(
                function ($model) use ($data, &$strInstance) {
                    switch (get_class($model)) {
                        case Lead::class:
                            $strInstance = 'leads';
                            $this->linkContacts($model, $data);
                            $this->linkCompany($model, $data);
                            break;

                        case Customer::class:
                            $strInstance = 'customers';
                            $this->linkContacts($model, $data);
                            $this->linkCompany($model, $data);
                            break;

                        case Company::class:
                            $strInstance = 'companies';
                            $this->linkContacts($model, $data);
                    }
                }
            );

            if ($strInstance) {
                $client->{$strInstance}->save($baseCollect);
            }
        }
    }

    protected function linkContacts($model, $data)
    {
        $contacts = $data['contacts'] ?? new Collection();
        if ($contacts->isNotEmpty()) {
            $model->link($contacts);
        }
    }

    protected function linkCompany($model, $data)
    {
        $company = ($data['companies'] ?? collect())->first();
        if (isset($company)) {
            $model->link(new Collection([$company]));
        }
    }

    public static function name(): string
    {
        return 'Связать сущности';
    }

    public static function statically(): array
    {
        return [
            'inner' => [
                'text' => 'Сущность',
                'items' => [
                    [
                        'id' => 'leads',
                        'name' => 'Сделка',
                        'deep' => [
                            'type' => 'select',
                            'extends' => 'deep|pull',
                            'items' => [],
                        ],
                    ],
                    [
                        'id' => 'contacts',
                        'name' => 'Контакт',
                        'deep' => [
                            'type' => 'select',
                            'extends' => 'deep|pull',
                            'items' => [],
                        ],
                    ],
                    [
                        'id' => 'companies',
                        'name' => 'Компания',
                        'deep' => [
                            'type' => 'select',
                            'extends' => 'deep|pull',
                            'items' => [],
                        ],
                    ],
                    [
                        'id' => 'customers',
                        'name' => 'Покупатель',
                        'deep' => [
                            'type' => 'select',
                            'extends' => 'deep|pull',
                            'items' => [],
                        ],
                    ],
                ],
            ],
//            'hide_top_value' => false,
            'hide_bottom_value' => true,
        ];
    }

    public static function forDynamically(): ?string
    {
        return null;
    }

}
