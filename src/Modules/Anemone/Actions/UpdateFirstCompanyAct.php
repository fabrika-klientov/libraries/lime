<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.13
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Actions;

use Anemone\Models\Company;
use Lime\Contracts\BeAction;
use Lime\Helpers\StateData;
use Lime\Modules\Anemone\Advanced\Controlling;

class UpdateFirstCompanyAct extends BaseAct implements BeAction
{
    use Controlling, StateData;

    public function handle($inner, array $deep = [])
    {
        $module = $this->modulesService->getModule($inner->module_uuid);
        if (empty($inner->inner) || empty($module)) {
            return;
        }

        $last = last($deep);
        $collect = $this->getFromState($inner, $deep, 'companies');
        $first = isset($collect) ? $collect->first() : ($last instanceof Company ? $last : null);

        if (empty($first)) {
            return;
        }

        $this->injectingData($first, $inner, $deep);

        $first->save(); // maybe move in end all rules
    }

    public static function name(): string
    {
        return 'Обновить первую найденную компанию';
    }

    public static function statically(): array
    {
        return [
            'deep' => [
                'type' => 'select',
                'extends' => 'deep|pull', // optionally (deep|pull...)
                'items' => [],
            ],
            'inner' => [
                'extends' => 'dynamically', // optional extends items
                'text' => 'Параметр',
                'items' => [], // for merging
            ],
            'replace' => true,
        ];
    }

    public static function forDynamically(): ?string
    {
        return 'company';
    }
}
