<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.15
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Actions;

use Anemone\Core\Collection\Collection;
use Anemone\Models\Call;
use Lime\Contracts\BeAction;
use Lime\Helpers\StateData;
use Lime\Modules\Anemone\Advanced\Controlling;

class StoreCallAct extends BaseAct implements BeAction
{
    use Controlling, StateData;

    public function handle($inner, array $deep = [])
    {
        $module = $this->modulesService->getModule($inner->module_uuid);
        if (empty($inner->inner) || empty($module)) {
            return;
        }

        $entity = new Call($module->getClient()->calls);

        foreach ($inner->inner as $one) {
            [$alienModule, $alienValue] = $this->getMixedAlien($one, $deep);
            if (empty($alienValue)) {
                continue;
            }

            if (empty($one->values->topValue->prop->id)) {
                continue;
            }

            $entity->{$one->values->topValue->prop->id} = $alienValue;
        }

        $entity->save();

        $this->addStateData($inner, $deep, 'calls', new Collection([$entity]));
    }

    public static function name(): string
    {
        return 'Создать звонок';
    }

    public static function statically(): array
    {
        return [
            'deep' => [
                'type' => 'select',
                'extends' => 'deep|pull', // optionally (deep|pull...)
                'items' => [],
            ],
            'inner' => [
                'extends' => 'dynamically', // optional extends items
                'text' => 'Параметр',
                'items' => [], // for merging
            ],
            'replace' => false,
        ];
    }

    public static function forDynamically(): ?string
    {
        return 'call';
    }
}
