<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.13
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Actions;

use Anemone\Core\Collection\Collection;
use Anemone\Models\Company;
use Lime\Contracts\BeAction;
use Lime\Helpers\StateData;
use Lime\Modules\Anemone\Advanced\Controlling;

class StoreCompanyAct extends BaseAct implements BeAction
{
    use Controlling, StateData;

    public function handle($inner, array $deep = [])
    {
        $module = $this->modulesService->getModule($inner->module_uuid);
        if (empty($inner->inner) || empty($module)) {
            return;
        }

        $entity = new Company($module->getClient()->companies);

        $this->injectingData($entity, $inner, $deep);

        $entity->save(); // maybe move in end all rules

        $this->addStateData($inner, $deep, 'companies', new Collection([$entity]));
    }

    public static function name(): string
    {
        return 'Создать компанию';
    }

    public static function statically(): array
    {
        return [
            'deep' => [
                'type' => 'select',
                'extends' => 'deep|pull', // optionally (deep|pull...)
                'items' => [],
            ],
            'inner' => [
                'extends' => 'dynamically', // optional extends items
                'text' => 'Параметр',
                'items' => [], // for merging
            ],
            'replace' => true,
        ];
    }

    public static function forDynamically(): ?string
    {
        return 'company';
    }
}
