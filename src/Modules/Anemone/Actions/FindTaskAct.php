<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.15
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Actions;

use Lime\Contracts\BeAction;
use Lime\Helpers\StateData;
use Lime\Modules\Anemone\Advanced\Controlling;
use Lime\Modules\Anemone\Advanced\Finding;

class FindTaskAct extends BaseAct implements BeAction
{
    use Controlling, Finding, StateData;

    public function handle($inner, array $deep = [])
    {
        $module = $this->modulesService->getModule($inner->module_uuid);
        if (empty($inner->inner) || empty($module)) {
            return;
        }

        /**
         * @var \Anemone\Client $client
         * */
        $client = $module->getClient();
        $instance = $client->tasks;
        $result = $this->findEntities($instance, $inner, $deep);

        if ($result->isNotEmpty()) {
            $this->addStateData($inner, $deep, 'tasks', $result);
        }
    }

    public static function name(): string
    {
        return 'Найти задачу';
    }

    public static function statically(): array
    {
        return [
            'deep' => [
                'type' => 'select',
                'extends' => 'deep|pull', // optionally (deep|pull...)
                'items' => [],
            ],
            'inner' => [
                'extends' => 'dynamically', // optional extends items
                'text' => 'По чему искать',
                'items' => [], // for merging
            ],
            'operator' => [
                'type' => 'and',
                'name' => 'И',
                'blocked' => false,
            ],
            'replace' => false,
        ];
    }

    public static function forDynamically(): ?string
    {
        return 'task';
    }

}
