<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.15
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Actions;

use Illuminate\Support\Str;
use Lime\Contracts\BeAction;
use Lime\Helpers\StateData;
use Lime\Modules\Anemone\Advanced\Controlling;
use Lime\Modules\Anemone\Advanced\Finding;

class FindNoteAct extends BaseAct implements BeAction
{
    use Controlling, Finding, StateData;

    protected $typeNote = null;

    public function handle($inner, array $deep = [])
    {
        $module = $this->modulesService->getModule($inner->module_uuid);
        if (empty($inner->inner) || empty($module)) {
            return;
        }

        $instance = $this->getNoteInstance($inner, $deep);

        if (empty($instance)) {
            return;
        }

        $data = $instance->get();
        if (empty($data) || $data->isEmpty()) {
            return;
        }

        foreach ($inner->inner as $one) {
            if (empty($one->values->topValue->prop->id) ||
                in_array($one->values->topValue->prop->id, ['entity_type', 'entity'])) {
                continue;
            }

            [$alienModule, $alienValue] = $this->getMixedAlien($one, $deep);
            if (empty($alienValue)) {
                continue;
            }

            $codes = [$one->values->topValue->prop->id];
            if (strpos($one->values->topValue->prop->id, 'params') === 0) {
                $codes = ['params', Str::after($one->values->topValue->prop->id, 'params_')];
            }

            $data = $data->filter(
                fn($item) => count($codes) == 1
                    ? $item->{head($codes)} == $alienValue
                    : (isset($item->{head($codes)}[last($codes)]) && $item->{head($codes)}[last($codes)] == $alienValue)
            );
        }

        if ($data->isNotEmpty()) {
            $data->each(fn($item) => $item->entity_type = $this->typeNote);
            $this->addStateData($inner, $deep, 'notes', $data);
        }
    }

    public static function name(): string
    {
        return 'Найти примечание';
    }

    public static function statically(): array
    {
        return [
            'deep' => [
                'type' => 'select',
                'extends' => 'deep|pull', // optionally (deep|pull...)
                'items' => [],
            ],
            'inner' => [
                'extends' => 'dynamically', // optional extends items
                'text' => 'По чему искать',
                'items' => [], // for merging
            ],
            'operator' => [
                'type' => 'and',
                'name' => 'И',
                'blocked' => true,
            ],
            'replace' => false,
        ];
    }

    /**
     * @param mixed $inner
     * @param array $deep
     * @return \Anemone\Models\Instances\NotesInstance|null
     */
    protected function getNoteInstance($inner, $deep)
    {
        return collect($inner->inner)
            ->reduce(
                function ($result, $one) use ($inner, $deep) {
                    if (isset($result)) {
                        return $result;
                    }

                    switch ($one->values->topValue->prop->id) {
                        case 'entity_type':
                            [$alienModule, $alienValue] = $this->getMixedAlien($one, $deep);
                            if (empty($alienValue)) {
                                return $result;
                            }
                            $tmpInstance = $this->modulesService
                                ->getModule($inner->module_uuid)
                                ->getClient()
                                ->{$alienValue};
                            if (isset($tmpInstance) && method_exists($tmpInstance, 'notes')) {
                                $this->typeNote = $alienValue;
                                return $tmpInstance->notes();
                            }
                            break;

                        case 'entity':
                            [$alienModule, $alienValue] = $this->getMixedAlien($one, $deep);
                            if (empty($alienValue)) {
                                return $result;
                            }

                            if (is_object($alienValue) && method_exists($alienValue, 'notes')) {
                                $this->typeNote = (string)Str::of(get_class($alienValue))->afterLast('\\')->lower();
                                $this->typeNote = $this->typeNote == 'company' ? 'companies' : ($this->typeNote . 's');
                                return $alienValue->notes();
                            }
                            break;
                    }

                    return $result;
                },
                null
            );
    }

    public static function forDynamically(): ?string
    {
        return 'note';
    }

}
