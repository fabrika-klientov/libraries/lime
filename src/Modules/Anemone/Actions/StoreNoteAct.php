<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.15
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Actions;

use Anemone\Core\Collection\Collection;
use Anemone\Models\Note;
use Illuminate\Support\Str;
use Lime\Contracts\BeAction;
use Lime\Helpers\StateData;
use Lime\Modules\Anemone\Advanced\Controlling;

class StoreNoteAct extends BaseAct implements BeAction
{
    use Controlling, StateData;

    public function handle($inner, array $deep = [])
    {
        $module = $this->modulesService->getModule($inner->module_uuid);
        if (empty($inner->inner) || empty($module)) {
            return;
        }

        $entity = new Note(['params' => []]);

        $parent = null;

        foreach ($inner->inner as $one) {
            [$alienModule, $alienValue] = $this->getMixedAlien($one, $deep);
            if (empty($alienValue)) {
                continue;
            }

            if (!isset($one->values->topValue->prop->id)) {
                continue;
            }

            $code = $one->values->topValue->prop->id;
            if (strpos($one->values->topValue->prop->id, 'params') === 0) {
                $code = Str::after($one->values->topValue->prop->id, 'params_');
            }

            switch ($code) {
                case 'note_type':
                case 'entity_id':
                    $entity->{$code} = $alienValue;
                    break;

                case 'text':
                case 'uniq':
                case 'duration':
                case 'source':
                case 'link':
                case 'phone':
                case 'service':
                case 'status':
                case 'icon_url':
                case 'address':
                case 'longitude':
                case 'latitude':
                    $entity->params = array_merge($entity->params, [$code => $alienValue]);
                    break;

                case 'entity_type':
                case 'entity':
                    $parent = $alienValue;
                    break;
            }
        }

        if (empty($parent)) {
            return;
        }

        /**
         * @var \Anemone\Models\Instances\NotesInstance|null $instance
         * */
        $instance = null;
        $typeNote = null;
        if (is_object($parent) && method_exists($parent, 'notes')) {
            $instance = $parent->notes();
            $typeNote = (string)Str::of(get_class($parent))->afterLast('\\')->lower();
            $typeNote = $typeNote == 'company' ? 'companies' : ($typeNote . 's');
        } elseif (is_string($parent)) {
            $tmpInstance = $module->getClient()->{$parent};
            if (isset($tmpInstance) && method_exists($tmpInstance, 'notes')) {
                $instance = $tmpInstance->notes();
                $instance->entityID($entity->entity_id);
                $typeNote = $parent;
            }
        }

        if (empty($instance)) {
            return;
        }

        $instance->save(new Collection([$entity]));

        $entity->{'entity_type'} = $typeNote;

        $this->addStateData($inner, $deep, 'notes', new Collection([$entity]));
    }

    public static function name(): string
    {
        return 'Создать примечание';
    }

    public static function statically(): array
    {
        return [
            'deep' => [
                'type' => 'select',
                'extends' => 'deep|pull', // optionally (deep|pull...)
                'items' => [],
            ],
            'inner' => [
                'extends' => 'dynamically', // optional extends items
                'text' => 'Параметр',
                'items' => [
                    ['id' => 'entity', 'name' => 'Сущность'],
                ], // for merging
            ],
            'replace' => false,
        ];
    }

    public static function forDynamically(): ?string
    {
        return 'note';
    }
}
