<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.15
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Anemone\Actions;

use Anemone\Models\Task;
use Lime\Contracts\BeAction;
use Lime\Helpers\StateData;
use Lime\Modules\Anemone\Advanced\Controlling;

class UpdateFirstTaskAct extends BaseAct implements BeAction
{
    use Controlling, StateData;

    public function handle($inner, array $deep = [])
    {
        $module = $this->modulesService->getModule($inner->module_uuid);
        if (empty($inner->inner) || empty($module)) {
            return;
        }

        $last = last($deep);
        $collect = $this->getFromState($inner, $deep, 'tasks');
        $first = isset($collect) ? $collect->first() : ($last instanceof Task ? $last : null);

        if (empty($first)) {
            return;
        }

        $this->injectingData($first, $inner, $deep);

        $first->save(); // maybe move in end all rules
    }

    public static function name(): string
    {
        return 'Обновить первую найденную задачу';
    }

    public static function statically(): array
    {
        return [
            'deep' => [
                'type' => 'select',
                'extends' => 'deep|pull', // optionally (deep|pull...)
                'items' => [],
            ],
            'inner' => [
                'extends' => 'dynamically', // optional extends items
                'text' => 'Параметр',
                'items' => [
                    ['id' => 'entity', 'name' => 'Сущность'],
                ], // for merging
            ],
            'replace' => false,
        ];
    }

    public static function forDynamically(): ?string
    {
        return 'task';
    }
}
