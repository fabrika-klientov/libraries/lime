<?php
/**
 * @package   Lime
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules;

use Lantana\Models\JSSharedCustomers;
use Lantana\Models\JSSharedModules;
use Lime\Helpers\DeepEntity;

abstract class BaseModule
{
    use DeepEntity;

    /**
     * @var array $REGISTERED
     * */
    protected static $REGISTERED = [];
    /**
     * @var string $table
     * */
    protected static $table = 'shared-modules';

    /**
     * @override
     * @param string|null $type
     * @param BaseModule|\Lime\Contracts\BeModule|null $context
     * @return \Illuminate\Support\Collection
     */
    public static function statically(string $type = null, $context = null)
    {
        switch ($type) {
            case 'condition':
                return static::staticallyConditions($context);
            case 'action':
                return static::staticallyActions($context);
            default:
                return static::staticallyConditions($context)->merge(static::staticallyActions($context));
        }
    }

    /**
     * @param BaseModule|\Lime\Contracts\BeModule|null $context
     * @return \Illuminate\Support\Collection
     */
    public static function staticallyConditions($context = null)
    {
        return collect(static::$REGISTERED['conditions'] ?? [])
            ->map(fn($class) => static::additionalStatically($class, $context));
    }

    /**
     * @param BaseModule|\Lime\Contracts\BeModule|null $context
     * @return \Illuminate\Support\Collection
     */
    public static function staticallyActions($context = null)
    {
        return collect(static::$REGISTERED['actions'] ?? [])
            ->map(fn($class) => static::additionalStatically($class, $context));
    }

    /**
     * @param \Lime\Contracts\BeCondact|string $class
     * @param BaseModule|\Lime\Contracts\BeModule|null $context
     * @return array
     */
    protected static function additionalStatically($class, $context = null)
    {
        return array_merge(
            [
                'type' => $class::TYPE,
                'module' => $class::module(),
                'module_uuid' => $context->getUUID() ?? null,
                'name' => $class::name(),
                'code' => $class,
                // .. add other in future
            ],
            $class::statically(),
            isset($context) ? ['dynamically' => $context->dynamically($class)] : []
        );
    }

    /** get dynamic data from module
     * @param string|null $condact
     * @return array
     */
    protected abstract function dynamically($condact = null): array;

    /**
     * @param JSSharedCustomers $customer
     * @param mixed $parent
     * @return JSSharedModules
     *
     * @throws \Lantana\Core\Exceptions\LantanaException
     */
    protected static function firstInit(JSSharedCustomers $customer, $parent)
    {
        $module = new JSSharedModules();
        $module->power = false;
        $module->customer_uuid = $customer->uuid;
        $module->save();

        $parent->{static::$table . '_uuid'} = $module->uuid;
        $parent->save();

        if (!isset($parent->_embedded)) {
            $parent->_embedded = (object)[];
        }
        $parent->_embedded->{static::$table} = $module;

        return $module;
    }

    /**
     * @param JSSharedCustomers $customer
     * @param \Illuminate\Support\Collection $collect
     * @return void
     * */
    protected static function controlling(JSSharedCustomers $customer, $collect)
    {
        $collect
            ->filter(fn($item) => !isset($item->{static::$table}))
            ->each(fn($item) => static::firstInit($customer, $item));
    }
}
