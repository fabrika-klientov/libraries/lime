<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.02
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Lime;

use Lantana\Models\JSSharedCustomers;
use Lime\Contracts\BeConverting;
use Lime\Contracts\BeModule;
use Lime\Modules\BaseModule;
use Lime\Modules\Lime\Advanced\Converting;
use Lime\Services\DataStateService;

class Module extends BaseModule implements BeModule
{
    /**
     * @inheritDoc
     * */
    protected static $REGISTERED = [
        'conditions' => [],
        'actions' => [
            \Lime\Modules\Lime\Actions\HandleCollectionAct::class,
            \Lime\Modules\Lime\Actions\ContinueAct::class,
            \Lime\Modules\Lime\Actions\BreakAct::class,
            \Lime\Modules\Lime\Actions\InitPullDataAct::class,
            \Lime\Modules\Lime\Actions\DestroyPullDataAct::class,
            \Lime\Modules\Lime\Actions\RunAnotherRule::class,
        ],
    ];
    /**
     * @var JSSharedCustomers $customer
     */
    protected $customer;
    /**
     * @var Converting $converting
     */
    protected $converting;

    /**
     * @param JSSharedCustomers $customer
     */
    private function __construct(JSSharedCustomers $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @inheritDoc
     * */
    protected function dynamically($condact = null): array
    {
        return [];
    }

    /**
     * @inheritDoc
     * */
    public static function name(): string
    {
        return 'Системный';
    }

    /**
     * @inheritDoc
     * */
    public static function description(): string
    {
        return 'System module description';
    }

    /**
     * @inheritDoc
     * */
    public static function icon(): string
    {
        return '';
    }

    /**
     * @inheritDoc
     * */
    public function getPower(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     * */
    public function setPower(bool $power): void
    {
        // TODO: Implement setPower() method.
    }

    /**
     * @inheritDoc
     * */
    public function getID()
    {
        return 'lime';
    }

    /**
     * @inheritDoc
     * */
    public function getUUID()
    {
        return 'system_lime';
    }

    /**
     * @inheritDoc
     * */
    public function getCustomerUUID()
    {
        return $this->customer->uuid;
    }

    /**
     * @inheritDoc
     * */
    public function getClient()
    {
        return null;
    }

    /**
     * @inheritDoc
     * */
    public function getValueFor(DataStateService $dataStateService, $bottomValue, array $injectDeep)
    {
        if (!isset($bottomValue->prop->id)) {
            return null;
        }

        switch ($bottomValue->prop->id) {

            case 'custom_value':
                return $bottomValue->prop->{'value'} ?? null;

            case 'item_iterator':
                if (empty($bottomValue->prop->value->module_uuid) ||
                    empty($bottomValue->prop->value->id) ||
                    empty($bottomValue->prop->value->top_code)
                ) {
                    return null;
                }

                /**
                 * @var \Lime\Contracts\BeModule|null $module
                 * */
                $module = $dataStateService->getModules($bottomValue->prop->value->module_uuid);
                if (empty($module)) {
                    return null;
                }

                return $module->getValueFor($dataStateService, $bottomValue, $injectDeep);

            default:
                // converted data
                if (strpos($bottomValue->prop->id, 'lime') === 0) {
                    return $this->getConverting($dataStateService)->getValue($bottomValue, $injectDeep);
                }

                return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function getConverting(DataStateService $dataStateService): BeConverting
    {
        if (!isset($this->converting)) {
            $this->converting = new Converting($dataStateService);
        }

        return $this->converting;
    }

    /**
     * @inheritDoc
     * */
    public function fullDynamically(): array
    {
        return [
            'base' => [
                ['id' => 'custom_value', 'name' => 'Пользовательское значение'],
                ['id' => 'item_iterator', 'name' => 'Элемент итерации (цикла)'],
            ],
        ];
    }

    /**
     * @inheritDoc
     * */
    public static function init(JSSharedCustomers $customer)
    {
        return collect([new self($customer)]);
    }

}
