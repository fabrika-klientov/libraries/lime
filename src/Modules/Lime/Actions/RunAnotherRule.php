<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.05
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Lime\Actions;

use Lantana\Models\JSLimeRules;
use Lime\Contracts\BeAction;
use Lime\Exceptions\LimeStatusException;
use Lime\Helpers\Logging;

class RunAnotherRule extends BaseAct implements BeAction
{
    use Logging;

    /**
     * @inheritDoc
     * */
    public function handle($inner, array $deep = [])
    {
        $uuidRule = $inner->deep->items[0]->uuid ?? null;
        $nameRule = $inner->deep->items[0]->name ?? null;

        // if not selected
        if (empty($uuidRule)) {
            static::logger(
                'warning',
                self::customerLogStr() . 'Another rule not selected',
                self::customerLogContextData()
            );
            return;
        }

        $rule = JSLimeRules::where('shared-customers_uuid', $this->modulesService->getCustomer()->uuid)
            ->find($uuidRule);

        // if not found
        if (empty($rule)) {
            static::logger(
                'warning',
                self::customerLogStr() . 'For UUID [' . $uuidRule . '][' . $nameRule . '] rule not found',
                self::customerLogContextData(['lime_rule_uuid' => $uuidRule, 'lime_rule_name' => $nameRule])
            );
            return;
        }

        // if not valid event
        if ($rule->event != 'App\Events\InnerRuleEvent') {
            static::logger(
                'warning',
                self::customerLogStr() . 'For UUID [' . $uuidRule . '][' . $nameRule
                . '] rule should be triggered by a "App\Events\InnerRuleEvent" event',
                self::customerLogContextData(['lime_rule_uuid' => $uuidRule, 'lime_rule_name' => $nameRule])
            );
            return;
        }

        // if not active
        if (!$rule->status) {
            static::logger(
                'warning',
                self::customerLogStr() . 'For UUID [' . $uuidRule . '][' . $nameRule . '] rule is inactive',
                self::customerLogContextData(['lime_rule_uuid' => $uuidRule, 'lime_rule_name' => $nameRule])
            );
            return;
        }

        // circular detected
        if ($this->dataStateService->hasAnotherRule($uuidRule)) {
            static::logger(
                'critical',
                self::customerLogStr() . 'For UUID [' . $uuidRule . ']['
                . $nameRule . '] detected circular trigger rules',
                self::customerLogContextData(['lime_rule_uuid' => $uuidRule, 'lime_rule_name' => $nameRule])
            );
            return;
        }

        static::logger(
            'info',
            self::customerLogStr() . 'Run Another Rule: UUID [' . $uuidRule . '][' . $nameRule . ']',
            self::customerLogContextData(['lime_rule_uuid' => $uuidRule, 'lime_rule_name' => $nameRule])
        );

        $this->dataStateService->addAnotherRule($uuidRule); // blocking the rule for circulars

        try {
            (new HandleCollectionAct($this->dataStateService, $this->modulesService))
                ->handleInner($rule, $deep, false);
        } catch (LimeStatusException $exception) {
            $this->dataStateService->removeAnotherRule($uuidRule); // free the rule

            throw $exception;
        }

        $this->dataStateService->removeAnotherRule($uuidRule); // free the rule
    }

    /**
     * @inheritDoc
     * */
    public static function name(): string
    {
        return 'Вызвать вложенный сценарий';
    }

    /**
     * @inheritDoc
     * */
    public static function statically(): array
    {
        return [
            'deep' => [
                'type' => 'select',
                'extends' => 'rules.event:App\\Events\\InnerRuleEvent', // optionally | - separate, . - filter
                'items' => [],
            ],
        ];
    }

    /**
     * @inheritDoc
     * */
    public static function forDynamically(): ?string
    {
        return null;
    }

    /**
     * @return string
     * */
    protected function customerLogStr()
    {
        return '[D] Customer [' . $this->modulesService->getCustomer()->uuid . '] >> ';
    }

    /**
     * @param array $merge
     * @return array
     */
    protected function customerLogContextData(array $merge = []): array
    {
        return array_merge(
            [
                'customer_uuid' => $this->modulesService->getCustomer()->uuid,
                'customer_name' => $this->modulesService->getCustomer()->{'name'},
            ],
            $merge
        );
    }

}
