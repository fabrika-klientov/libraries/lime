<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.22
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Lime\Actions;

use Lime\Contracts\BeAction;

class InitPullDataAct extends BaseAct implements BeAction
{

    public function handle($inner, array $deep = [])
    {
        $this->dataStateService->initPullData(count($deep) - 1);
    }

    public static function name(): string
    {
        return 'Инициализировать пул данных (изоляция)';
    }

    public static function statically(): array
    {
        return [];
    }

    public static function forDynamically(): ?string
    {
        return null;
    }
}
