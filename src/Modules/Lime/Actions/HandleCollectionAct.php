<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.04
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Lime\Actions;

use Lantana\Models\JSLimeCondact;
use Lantana\Models\JSLimeRules;
use Lime\Contracts\BeAction;
use Lime\Contracts\BeCondact;
use Lime\Contracts\BeModule;
use Lime\Exceptions\LimeStatusException;
use Lime\Helpers\Logging;
use Lime\Services\DataStateService;

class HandleCollectionAct extends BaseAct implements BeAction
{
    use Logging;

    /**
     * @var JSLimeRules $rule
     * */
    private $rule;
    /**
     * @var \Illuminate\Support\Collection $condact
     * */
    private $condact;
    /**
     * @var int $collectTime
     * */
    protected $collectTime;
    /**
     * @var int $ruleTime
     * */
    protected $ruleTime;
    /**
     * @var int $countCondacts
     * */
    protected $countCondacts = 0;
    /**
     * @var bool $showMetric
     * */
    protected $showMetric = true;


    public function handle($inner, array $deep = [])
    {
        $this->collectTime = time();

        if (empty($inner->deep->items) || empty($inner->inner)) {
            return;
        }

        [$firstItem] = $inner->deep->items;
        [$firstInner] = $inner->inner;

        if (empty($firstItem->type) || empty($firstInner->rule_uuid)) {
            return;
        }

        $rule = JSLimeRules::where('shared-customers_uuid', $this->modulesService->getCustomer()->uuid)
            ->where('uuid', $firstInner->rule_uuid)
            ->belongsToLimeCondact()
            ->first();

        if (empty($rule)) { // for collect action inner rule not found
            return;
        }

        switch ($firstItem->type) {
            case 'system_lime_repeat':
                $count = (int)($firstItem->deep->value ?? 0);
                if ($count <= 0) {
                    return;
                }
                $collection = collect(range(0, $count - 1));
                break;

            default:
                $module = $this->modulesService->getModule($firstItem->module_uuid);
                if (isset($module)) {
                    $collection = $module->getValueFor(
                        $this->dataStateService,
                        (object)['prop' => (object)['id' => 'system_collection', 'top_code' => $firstItem->type]],
                        $deep
                    );
                }

                if (empty($collection) || $collection->isEmpty()) {
                    return;
                }
        }

        foreach ($collection as $item) {
            try {
                $this->dataStateService->initDeepData(count($deep));
                $this->handleInner($rule, array_merge($deep, [$item]), false);
                $this->dataStateService->destroyDeepData(count($deep));
            } catch (LimeStatusException $exception) {
                $this->dataStateService->destroyDeepData(count($deep));
                switch ($exception->getCode()) {
                    case LimeStatusException::CONTINUE_COLLECT_ACT:
                        continue 2;
                    case LimeStatusException::BREAK_COLLECT_ACT:
                        break 2;
                }
            }
        }
        // collection finished
    }

    /**
     * @param JSLimeRules $rule
     * @param array $deep
     * @param bool $showMetric
     * @return void
     * @throws \Exception
     */
    public function handleInner(JSLimeRules $rule, array $deep = [0], bool $showMetric = true)
    {
        $this->showMetric = $showMetric;
        $this->ruleTime = time();

        $this->rule = $rule;
        $this->condact = $rule->limeCondact();
        if ($this->condact->isEmpty()) {
            // skip (for rule condact list is empty)
            self::logMetric();
            return;
        }

        $item = null;
        $conRes = null;
        try {
            while ($item = $this->next($item, $conRes)) {
                $conRes = $this->executeCondact($item, $deep);
            }
        } catch (\Exception $exception) {
            self::logMetric();

            throw $exception;
        }

        self::logMetric();
        // finish all condact of rule
    }

    /**
     * @param JSLimeCondact $item
     * @param array $deep
     * @return bool|null
     * @throws \Exception
     */
    protected function executeCondact(JSLimeCondact $item, array $deep): ?bool
    {
        ++$this->countCondacts;

        self::condactMetric($item);

        $inner = collect($item->inner);
        if ($inner->isEmpty()) {
            return $item->type == 'action' ? null : true;
        }

        $helper = function ($one) use ($deep) {
            return class_exists($one->code ?? null) && is_subclass_of($one->code, BeCondact::class)
                ? (new $one->code($this->dataStateService, $this->modulesService))->handle($one, $deep)
                : 0;
        };

        switch ($item->type) {
            case 'action':
                $inner->each($helper);
                return null;
            case 'condition':
                return $inner->every($helper); // [And]
        }

        throw new \Exception('Condact should be type of ENUM("action", "condition")');
    }

    /**
     * @param JSLimeCondact|null $item
     * @param bool|null $condRes
     * @return JSLimeCondact|null
     */
    protected function next(JSLimeCondact $item = null, bool $condRes = null): ?JSLimeCondact
    {
        return $this->condact->first(
            function (JSLimeCondact $one) use ($item, $condRes) {
                return isset($item)
                    ? ($one->extends == $item->uuid && (isset($condRes) ? $one->extendsCondStatus === $condRes : true))
                    : is_null($one->extends);
            }
        );
    }

    /**
     * @return void
     * */
    protected function logMetric()
    {
        if (isset($this->collectTime)) { // break if collect
            return;
        }

        if (!$this->showMetric) { // skip if set off metric (ex.: collect, another rule)
            return;
        }

        $endTime = time();
        $deltaTime = $endTime - $this->ruleTime;

        static::logger(
            'info',
            '[S] Customer [' . $this->modulesService->getCustomer()->uuid
            . '] >> Rule [' . $this->rule->uuid . '] (' . $this->rule->name . ')',
            [
                'customer_uuid' => $this->modulesService->getCustomer()->uuid,
                'customer_name' => $this->modulesService->getCustomer()->{'name'},
                'lime_rule_uuid' => $this->rule->uuid,
                'lime_rule_name' => $this->rule->name,
                'lime_statistic_time' => $deltaTime,
                'lime_statistic_time_start' => date('Y-m-d H:i:s', $this->ruleTime),
                'lime_statistic_time_end' => date('Y-m-d H:i:s', $endTime),
                'lime_statistic_condact_count' => $this->countCondacts,
                'lime_statistic_modules_count' => $this->modulesService->getModules()->count(),
                'lime_statistic_modules_count_active' => $this->modulesService
                    ->getModules()
                    ->reduce(fn(int $result, BeModule $module) => $module->getPower() ? ++$result : $result, 0),
            ]
        );
    }

    /**
     * @param JSLimeCondact $item
     * @return void
     */
    protected function condactMetric(JSLimeCondact $item)
    {
        $char = $item->type == 'action' ? 'A' : ($item->type == 'condition' ? 'C' : '?');

        $inner = collect($item->inner);

        $first = $inner->first();
        /**
         * @var BeCondact|null $condact
         * */
        $condact = isset($first) && class_exists($first->code ?? null) && is_subclass_of($first->code, BeCondact::class)
            ? new $first->code($this->dataStateService, $this->modulesService)
            : null;

        static::logger(
            'info',
            '[' . $char . '] Customer [' . $this->modulesService->getCustomer()->uuid
            . '] >> Rule [' . $this->rule->uuid . '] (' . $this->rule->name
            . ') - Condact [' . $item->uuid . '] (' . $item->name . ')',
            [
                'customer_uuid' => $this->modulesService->getCustomer()->uuid,
                'customer_name' => $this->modulesService->getCustomer()->{'name'},
                'lime_rule_uuid' => $this->rule->uuid,
                'lime_rule_name' => $this->rule->name,
                'lime_condact_uuid' => $item->uuid,
                'lime_condact_name' => $item->name,
                'lime_condact_type' => $item->type,
                'lime_condact_module' => isset($condact) ? $condact->module() : null,
                'lime_condact_static_name' => isset($condact) ? $condact->name() : null,
                'lime_condact_class' => isset($condact) ? get_class($condact) : null,
            ]
        );
    }

    public static function name(): string
    {
        return 'Обработать коллекцию (цикл)';
    }

    public static function statically(): array
    {
        return [
            'deep' => [
                'type' => 'select_for_module',
                'items' => array_merge(
                    [
                        'Системный' => [
                            [
                                'type' => 'system_lime_repeat',
                                'name' => 'Повторить N раз',
                                'deep' => [
                                    'type' => 'input',
                                ],
                            ],
                            // your data
                        ],
                    ],
                    DataStateService::getDataSkeleton()['collection']
                )
            ]
        ];
    }

    public static function forDynamically(): ?string
    {
        return null;
    }

}
