<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.05
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Lime\Actions;

use Lime\Contracts\BeAction;
use Lime\Exceptions\LimeStatusException;

class ContinueAct extends BaseAct implements BeAction
{
    /**
     * @param $inner
     * @param array $deep
     * @throws LimeStatusException
     */
    public function handle($inner, array $deep = [])
    {
        throw new LimeStatusException('Continue', LimeStatusException::CONTINUE_COLLECT_ACT);
    }

    public static function name(): string
    {
        return 'Прервать текущую итерацию';
    }

    public static function statically(): array
    {
        return [];
    }

    public static function forDynamically(): ?string
    {
        return null;
    }

}
