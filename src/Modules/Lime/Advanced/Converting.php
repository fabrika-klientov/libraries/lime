<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.25
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Lime\Advanced;

use Illuminate\Support\Str;
use Lantana\Models\JSLimeConvert;
use Lime\Contracts\BeConverting;
use Lime\Contracts\BeExecuteConvert;
use Lime\Modules\Lime\Advanced\Convert\CollectToString;
use Lime\Services\DataStateService;

class Converting implements BeConverting
{
    private DataStateService $dataStateService;

    /**
     * @param DataStateService $dataStateService
     */
    public function __construct(DataStateService $dataStateService)
    {
        $this->dataStateService = $dataStateService;
    }

    /**
     * @inheritDoc
     */
    public function getValue($bottomValue, array $deep)
    {
        switch ($bottomValue->prop->id) {
            case 'lime_object_to_string':
            case 'lime_array_to_string':
            case 'lime_format_phone':
                if (empty($bottomValue->prop->deep->bottomValue)) {
                    return null;
                }

                $module = $this->dataStateService->getModules($bottomValue->prop->deep->bottomValue->module->uuid);
                if (empty($module)) {
                    return null;
                }

                $data = $module->getValueFor($this->dataStateService, $bottomValue->prop->deep->bottomValue, $deep);
                if (empty($data)) {
                    return null;
                }

                $convertRule = JSLimeConvert::where('type', $bottomValue->prop->id)
                    ->find($bottomValue->prop->value);
                if (empty($convertRule)) {
                    return null;
                }

                return $this->getConvert($bottomValue->prop->id)->handle($convertRule, $data);
        }

        return null;
    }

    /**
     * @inheritDoc
     * */
    public function getPreparingData($bottomValue, array $deep)
    {
        throw new \Exception('Not prepare');
    }

    /**
     * @inheritDoc
     * */
    public function getConvert(string $code): ?BeExecuteConvert
    {
        switch ($code) {
            case 'lime_object_to_string':
            case 'lime_format_phone':
                $class = 'Lime\\Modules\\Lime\\Advanced\\Convert\\'
                    . Str::of($code)->after('lime_')->camel()->ucfirst();
                if (class_exists($class)) {
                    return new $class();
                }
                break;

            case 'lime_array_to_string':
                return new CollectToString();
        }

        return null;
    }

}
