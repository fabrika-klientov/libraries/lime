<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.04
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Lime\Advanced\Convert;

use Lantana\Models\JSLimeConvert;
use Lime\Contracts\BeExecuteConvert;

class ObjectToString implements BeExecuteConvert
{

    public function __construct()
    {
    }

    /**
     * @inheritDoc
     * */
    public function handle(JSLimeConvert $limeConvert, $entity = null)
    {
        if (empty($limeConvert->deep->keys)) {
            return '';
        }

        return preg_replace_callback(
            '/{#(\d+)}/i',
            function ($matches) use ($limeConvert, $entity) {
                if (isset($limeConvert->deep->keys[$matches[1] - 1])) {
                    $key = $limeConvert->deep->{'keys'}[$matches[1] - 1];

                    if (is_object($entity)) {
                        if (method_exists($entity, $key)) {
                            return $entity->{$key}();
                        }

                        return $entity->{$key} ?? '';
                    } elseif (is_array($entity)) {
                        return $entity[$key] ?? '';
                    }
                }

                return '';
            },
            $limeConvert->deep->{'template'}
        );
    }
}
