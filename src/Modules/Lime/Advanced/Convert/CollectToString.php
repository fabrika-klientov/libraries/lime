<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.04
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Lime\Advanced\Convert;

use Lantana\Models\JSLimeConvert;
use Lime\Contracts\BeExecuteConvert;

class CollectToString implements BeExecuteConvert
{

    protected ObjectToString $objectToString;

    public function __construct()
    {
        $this->objectToString = new ObjectToString();
    }

    /**
     * @inheritDoc
     * */
    public function handle(JSLimeConvert $limeConvert, $collect = null)
    {
        if (empty($limeConvert->deep->template_global)) {
            return '';
        }

        $extendLimeConvert = empty($limeConvert->deep->lime_object_to_string_uuid)
            ? null
            : JSLimeConvert::find($limeConvert->deep->lime_object_to_string_uuid);

        $index = 0;
        $helper = function ($item) use ($limeConvert, $extendLimeConvert, &$index) {
            $objToStr = isset($extendLimeConvert) ? $this->objectToString->handle($extendLimeConvert, $item) : '';
            ++$index;

            return preg_replace_callback(
                '/\{(##|index|br)\}/i',
                function ($matches) use ($objToStr, $index) {
                    switch ($matches[1]) {
                        case '##':
                            return $objToStr;

                        case 'index':
                            return $index;

                        case 'br':
                            return PHP_EOL;
                    }

                    return '';
                },
                $limeConvert->deep->{'template'}
            );
        };

        $result = join(
            '',
            is_array($collect) ? array_map($helper, $collect) : $collect->map($helper)->toArray()
        );

        return preg_replace_callback(
            '/\{(merge)\}/i',
            function ($matches) use ($result) {
                switch ($matches[1]) {
                    case 'merge':
                        return $result;
                }

                return '';
            },
            $limeConvert->deep->{'template_global'}
        );
    }
}
