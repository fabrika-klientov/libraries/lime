<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.04
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules\Lime\Advanced\Convert;

use Lantana\Models\JSLimeConvert;
use Lime\Contracts\BeExecuteConvert;

class FormatPhone implements BeExecuteConvert
{

    public function __construct()
    {
    }

    /**
     * @inheritDoc
     * */
    public function handle(JSLimeConvert $limeConvert, $additional = null)
    {
        if (empty($limeConvert->deep->code) && empty($limeConvert->deep->format)) {
            return $additional;
        }

        $formatted = empty($limeConvert->deep->code)
            ? $additional
            : self::formatting((string)$limeConvert->deep->code, (string)$additional);

        return empty($limeConvert->deep->format)
            ? $formatted
            : self::templating((string)$limeConvert->deep->format, $formatted);
    }

    /**
     * @param string $code
     * @param string $phone
     * @return string
     */
    protected function formatting(string $code, string $phone)
    {
        return $code . self::getTrimPhone($code, $phone);
    }

    /**
     * @param string $format
     * @param string $formattedPhone
     * @return string
     */
    protected function templating(string $format, string $formattedPhone)
    {
        return collect(str_split($formattedPhone))
            ->reduce(fn($result, $one) => preg_replace('/#/', $one, $result, 1), $format);
    }

    /**
     * @param string $code
     * @param string $phone
     * @return string
     */
    protected function getTrimPhone(string $code, string $phone)
    {
        switch ($code) {
            case '7':
                $last = 10;
                break;

            default:
                $last = 9;
        }

        return substr(self::onlyNumbers($phone), -$last);
    }

    /**
     * @param string $phone
     * @return string
     */
    protected function onlyNumbers(string $phone)
    {
        return preg_replace('/\D/', '', $phone);
    }
}
