<?php
/**
 *
 * */

namespace Lime\Modules\Crocus;

use Crocus\Adapters\OrdersAdapter;
use Crocus\Adapters\OrdersProductAdapter;
use Crocus\Client;
use Crocus\Models\Orders;
use Lantana\Models\JSBisqueConfigs;
use Lantana\Models\JSSharedCustomers;
use Lantana\Models\JSSharedCustomersServices;
use Lantana\Models\JSSharedModules;
use Lime\Contracts\BeConverting;
use Lime\Contracts\BeModule;
use Lime\Helpers\StateDataHelper;
use Lime\Modules\BaseModule;
use Lime\Modules\Crocus\Advanced\Converting;
use Lime\Services\DataStateService;

class Module extends BaseModule implements BeModule
{
    /**
     * @inheritDoc
     * */
    protected static $REGISTERED = [
        'conditions' => [],
        'actions' => [
            \Lime\Modules\Crocus\Actions\GetOrderListAct::class,
        ],
    ];
    /**
     * @var JSSharedModules $module
     * */
    private JSSharedModules $module;
    /**
     * @var JSBisqueConfigs $blackConfigs
     * */
    private JSBisqueConfigs $bisqueConfigs;
    /**
     * @var Converting $converting
     * */
    private $converting;

    public function __construct(JSSharedModules $module, JSBisqueConfigs $bisqueConfigs)
    {
        $this->module = $module;
        $this->bisqueConfigs = $bisqueConfigs;
    }

    /**
     * @inheritDoc
     */
    public static function name(): string
    {
        return 'crocus';
    }

    /**
     * @inheritDoc
     */
    public static function description(): string
    {
        return 'Description for Crocus';
    }

    /**
     * @inheritDoc
     */
    public static function icon(): string
    {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function getPower(): bool
    {
        return $this->module->power;
    }

    /**
     * @inheritDoc
     */
    public function setPower(bool $power): void
    {
        $this->module->power = $power;
        $this->module->save();
    }

    /**
     * @inheritDoc
     */
    public function getID()
    {
        return $this->bisqueConfigs->name;
    }

    /**
     * @inheritDoc
     */
    public function getUUID()
    {
        return $this->module->uuid;
    }

    /**
     * @inheritDoc
     */
    public function getCustomerUUID()
    {
        return $this->module->customer_uuid;
    }

    /**
     * @inheritDoc
     */
    public function getClient()
    {
        return new Client((array)$this->bisqueConfigs->auth);
    }

    /**
     * @inheritDoc
     */
    public function getValueFor(DataStateService $dataStateService, $bottomValue, array $injectDeep)
    {
        if (!isset($bottomValue->prop->id)) {
            return null;
        }

        $injectEntity = $this->getDeepEntity($injectDeep, $bottomValue);

        $switch = $bottomValue->prop->id;

        // for converting
        try {
            [$switch, $injectEntity] = $this
                ->getConverting($dataStateService)
                ->getPreparingData($bottomValue, $injectDeep);
        } catch (\Exception $e) {
        }

        switch ($switch) {
            case 'last_time_order':
                return $this->bisqueConfigs->last_fixed_date;

            case 'system_collection':
                switch ($bottomValue->prop->top_code) {
                    case 'orders':
                        $state = new StateDataHelper($this, $dataStateService);
//                        return $dataStateService->getForModule($this->getUUID(), $bottomValue->prop->top_code);
                        return $state->getFromState(null, $injectDeep, $bottomValue->prop->top_code);

                    case 'products':
                        if (isset($injectEntity) && $injectEntity instanceof Orders) {
                            return collect(
                                array_map(fn($item) => new OrdersProductAdapter($item), $injectEntity->products ?? [])
                            );
                        }
                }
                return null;

            case 'item_iterator': // always from Lime\Module
                switch ($bottomValue->prop->value->top_code) {
                    case 'orders':
                        if (!$injectEntity instanceof Orders) {
                            return null; // bug in construct rules
                        }
                        $adapter = new OrdersAdapter($injectEntity);
                        break;

                    case 'products':
                        if (!$injectEntity instanceof OrdersProductAdapter) {
                            return null; // bug in construct rules
                        }
                        $adapter = $injectEntity;
                        break;
                }

                if (isset($adapter) &&
                    $bottomValue->prop->id == 'item_iterator' && // for not converting
                    method_exists($adapter, $bottomValue->prop->value->id)
                ) {
                    return $adapter->{$bottomValue->prop->value->id}();
                }

                return $adapter ?? null;
        }

        // add for other first element as anemone

        return null;
    }

    /**
     * @inheritDoc
     * */
    public function getConverting(DataStateService $dataStateService): BeConverting
    {
        if (!isset($this->converting)) {
            $this->converting = new Converting($dataStateService);
        }

        return $this->converting;
    }


    /**
     * @inheritDoc
     */
    public function fullDynamically(): array
    {
        return [
            'base' => [
                ['id' => 'last_time_order', 'name' => 'Дата последнего обработанного заказа'],
            ],
            'orders' => [
                ['id' => 'getOrderId', 'name' => 'ID заказа'],
                ['id' => 'getDateCreated', 'name' => 'Дата создания заказа'],
                ['id' => 'getFirstName', 'name' => 'Имя клиента'],
                ['id' => 'getSecondName', 'name' => 'Отчество клиента'],
                ['id' => 'getLastName', 'name' => 'Фамилия клиента'],
                ['id' => 'getFullName', 'name' => 'Полное имя клиента'],
                ['id' => 'getEmail', 'name' => 'Email клиента'],
                ['id' => 'getPhone', 'name' => 'Телефон клиента'],
                ['id' => 'getDeliveryId', 'name' => 'ID способа доставки'],
                ['id' => 'getDeliveryType', 'name' => 'Название способа доставки'],
                ['id' => 'getDeliveryAddress', 'name' => 'Адрес доставки'],
                ['id' => 'getPaymentId', 'name' => 'ID способа оплаты'],
                ['id' => 'getPaymentType', 'name' => 'Название способа оплаты'],
                ['id' => 'getPrice', 'name' => 'Сумма заказа'],
                ['id' => 'getComment', 'name' => 'Комментарий'],
                ['id' => 'getStatus', 'name' => 'Статус заказа'],
                ['id' => 'getSource', 'name' => 'Источник заказа'],
            ],
            'products' => [
                ['id' => 'getId', 'name' => 'ID товара'],
                ['id' => 'getName', 'name' => 'Название товара'],
                ['id' => 'getPrice', 'name' => 'Цена товара'],
                ['id' => 'getQuantity', 'name' => 'Количество единиц'],
                ['id' => 'getTotalPrice', 'name' => 'Сумма заказа'],
                ['id' => 'getLink', 'name' => 'Ссылка на товар'],
                ['id' => 'getMeasureUnit', 'name' => 'Единица измерения'],
                ['id' => 'getArticle', 'name' => 'Артикул'],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    protected function dynamically($condact = null): array
    {
        return [];
    }

    /**
     * @inheritDoc
     * @throws \Lantana\Core\Exceptions\LantanaException
     */
    public static function init(JSSharedCustomers $customer)
    {
        $beforeCollect = JSSharedCustomersServices::where('shared-customers_uuid', $customer->uuid)
            ->where('code', 'bisque')
            ->get();

        if (empty($beforeCollect) || $beforeCollect->isEmpty()) {
            return collect();
        }

        $collect = JSBisqueConfigs::where('type', 'crocus')
            ->wherein(
                'shared-customers-services_uuid',
                $beforeCollect->pluck('uuid')->values()->toArray()
            )
            ->withSharedModules()
            ->get();

        if (empty($collect)) {
            return collect();
        }

        static::controlling($customer, $collect);

        return $collect->map(fn(JSBisqueConfigs $item) => new self($item->{static::$table}, $item));
    }

    /**
     * @return JSBisqueConfigs
     * */
    public function getConfigs()
    {
        return $this->bisqueConfigs;
    }

}
