<?php
/**
 * */

namespace Lime\Modules\Crocus\Actions;

use Crocus\Models\Orders;
use Lime\Contracts\BeAction;
use Lime\Helpers\StateData;
use Lime\Modules\Crocus\Advanced\Controlling;

class GetOrderListAct extends BaseAct implements BeAction
{
    use Controlling, StateData;

    public function handle($inner, array $deep = [])
    {
        /**
         * @var \Lime\Modules\Crocus\Module $module
         * */
        $module = $this->modulesService->getModule($inner->module_uuid);
        if (empty($inner->inner) || empty($module)) {
            return;
        }

        /**
         * @var \Crocus\Client $client
         * */
        $client = $module->getClient();

        $instance = $client->orders;

        // query closure
        foreach ($inner->inner as $one) {
            [$alienModule, $alienValue] = $this->getMixedAlien($one, $deep);
            if (empty($alienValue)) {
                continue;
            }

            if (empty($one->values->topValue->prop->id)) {
                continue;
            }

            $instance->{$one->values->topValue->prop->id}($alienValue);
        }

        // get orders
        $result = $instance->get();

        if ($result->isNotEmpty()) {
            $this->addStateData($inner, $deep, 'orders', $result);

            $config = $module->getConfigs();
            $newLastTime = $result->reduce(
                function($result, Orders $order) {
                    $new = strtotime($order->date_created);
                    if (empty($result)) {
                        return $new;
                    }

                    return $new > $result ? $new : $result;
                },
                isset($config->last_fixed_date) ? strtotime($config->last_fixed_date) : null
            );

            $config->last_fixed_date = date('Y-m-d H:i:s', $newLastTime + 1);
            $config->save();
        }
    }

    public static function name(): string
    {
        return 'Получить список заказов';
    }

    public static function statically(): array
    {
        return [
            'deep' => [
                'type' => 'select',
                'extends' => 'deep|pull', // optionally (deep|pull...)
                'items' => [],
            ],
            'inner' => [
                'text' => 'По чему искать',
                'items' => [
                    ['id' => 'status', 'name' => 'Статус заказа'],
                    ['id' => 'dateFrom', 'name' => 'После указанной даты'],
                    ['id' => 'dateTo', 'name' => 'До указанной даты'],
                    ['id' => 'limit', 'name' => 'Количество заказов'],
                    ['id' => 'lastId', 'name' => 'Идентификаторы не выше указанного'],
                ],
            ],
        ];
    }

    public static function forDynamically(): ?string
    {
        return null;
    }

}
