<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.02
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Modules;

use Lime\Services\DataStateService;
use Lime\Services\ModulesService;

abstract class BaseCond
{
    /**
     * @var DataStateService $dataStateService
     */
    protected $dataStateService;
    /**
     * @var ModulesService $modulesService
     */
    protected $modulesService;

    /**
     * @param DataStateService $dataStateService
     * @param ModulesService $modulesService
     */
    public function __construct(DataStateService $dataStateService, ModulesService $modulesService)
    {
        $this->dataStateService = $dataStateService;
        $this->modulesService = $modulesService;
    }

    public abstract static function module(): string;
}
