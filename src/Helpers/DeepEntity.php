<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.21
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Helpers;

trait DeepEntity
{

    /**
     * @param array $deep
     * @param mixed $bottomValue
     * @return mixed|null
     */
    protected function getDeepEntity(array $deep, $bottomValue)
    {
        if (empty($bottomValue->prop->id)) {
            return last($deep);
        }

        switch ($bottomValue->prop->id) {
            case 'item_iterator':
            case 'system_collection':
                return last($deep);

            case 'item_iterator_deep':
                return $deep[count($deep) - 1 + $bottomValue->prop->deep_value] ?? null;
        }

        return null;
    }
}
