<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.22
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Helpers;

use Lime\Contracts\BeModule;
use Lime\Services\DataStateService;

class StateDataHelper
{
    use StateData;

    private BeModule $module;
    private DataStateService $dataStateService;

    public function __construct(BeModule $module, DataStateService $service)
    {
        $this->module = $module;
        $this->dataStateService = $service;
    }

}
