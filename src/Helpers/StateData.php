<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.22
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Helpers;

use Illuminate\Support\Collection;

trait StateData
{
    /**
     * @param mixed $inner
     * @param array $deep
     * @param string $code
     * @param mixed $data
     * @param bool $isMerge
     * @return void
     */
    public function addStateData($inner, array $deep, string $code, $data, bool $isMerge = true)
    {
        $module = $this->module ?? $this->modulesService->getModule($inner->module_uuid);
        if (empty($module)) {
            return;
        }

        if (!empty($inner->deep->items)) {
            $deltaDeep = $inner->deep->items[0]->deep_value ?? null;
            $toDeep = isset($deltaDeep) ? (count($deep) - 1 + $deltaDeep) : 0;

            switch ($inner->deep->items[0]->type) {
                case 'top_deep':
                case 'lock_deep':
                    $before = $this->dataStateService->getForModule($module->getUUID(), $code, $toDeep);
                    $this->dataStateService->setForModule(
                        $module->getUUID(),
                        $code,
                        $isMerge ? static::_mergingData($before, $data) : $data,
                        $toDeep
                    );
                    return;

                case 'top_pull':
                case 'pull_deep':
                    $before = $this->dataStateService->getPullDataForModule($module->getUUID(), $code, $toDeep);
                    $this->dataStateService->setPullDataForModule(
                        $module->getUUID(),
                        $code,
                        $isMerge ? static::_mergingData($before, $data) : $data,
                        $toDeep
                    );
                    return;
            }
        }

        // if not selected (using privileges)
        $toDeep = count($deep) - 1;

        // to pull data
        $before = $this->dataStateService->getPullDataForModule($module->getUUID(), $code, $toDeep);
        $this->dataStateService->setPullDataForModule(
            $module->getUUID(),
            $code,
            $isMerge ? static::_mergingData($before, $data) : $data,
            $toDeep
        );

        $check = $this->dataStateService->getPullDataForModule($module->getUUID(), $code, $toDeep);
        if (isset($check)) {
            return;
        }

        // to lock data
        $before = $this->dataStateService->getForModule($module->getUUID(), $code, $toDeep);
        $this->dataStateService->setForModule(
            $module->getUUID(),
            $code,
            $isMerge ? static::_mergingData($before, $data) : $data,
            $toDeep
        );
    }

    /**
     * @param mixed $inner
     * @param array $deep
     * @param string $code
     * @param mixed $data
     * @return void
     */
    public function replaceStateData($inner, array $deep, string $code, $data)
    {
        $this->addStateData($inner, $deep, $code, $data, false);
    }

    /**
     * @param mixed $inner
     * @param array $deep
     * @param string $code
     * @return mixed|null
     */
    public function getFromState($inner, array $deep, string $code)
    {
        $module = $this->module ?? $this->modulesService->getModule($inner->module_uuid);
        if (empty($module)) {
            return null;
        }

        if (!empty($inner->deep->items)) {
            $deltaDeep = $inner->deep->items[0]->deep_value ?? null;
            $toDeep = isset($deltaDeep) ? (count($deep) - 1 + $deltaDeep) : 0;

            switch ($inner->deep->items[0]->type) {
                case 'top_deep':
                case 'lock_deep':
                    return $this->dataStateService->getForModule($module->getUUID(), $code, $toDeep);

                case 'top_pull':
                case 'pull_deep':
                    return $this->dataStateService->getPullDataForModule($module->getUUID(), $code, $toDeep);
            }
        }

        // if not selected (using privileges)
        $toDeep = count($deep) - 1;

        // get from pull data
        $data = $this->dataStateService->getPullDataForModule($module->getUUID(), null, $toDeep);
        if (isset($data)) { // maybe not initialized pull
            return $data[$code] ?? null;
        }

        // get from lock data
        return $this->dataStateService->getForModule($module->getUUID(), $code, $toDeep);
    }

    /**
     * @param mixed $before
     * @param mixed $push
     * @return array|Collection|mixed|null
     */
    private static function _mergingData($before, $push)
    {
        if ($before instanceof Collection && $push instanceof Collection) {
            return $before->merge($push);
        } elseif ($before instanceof Collection && is_array($push)) {
            return $before->push(...$push);
        } elseif (is_array($before) && is_array($push)) {
            return array_merge($before, $push);
        } elseif (is_array($before)) {
            $before[] = $push;
            return $before;
        }

        return $push;
    }
}
