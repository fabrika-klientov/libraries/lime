<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.04
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Helpers;

use Illuminate\Support\Str;

/** using outer logger (ex.: Laravel) in top namespace
 * */
trait Logging
{
    /**
     * @var array $LEVELS
     * */
    protected static $LEVELS = ['emergency', 'alert', 'critical', 'error', 'warning', 'notice', 'info', 'debug'];

    /**
     * @param string $level
     * @param mixed $data
     * @param array $context
     */
    protected static function logger(string $level, $data, array $context = [])
    {
        $logger = '\Log';
        if (class_exists($logger) && in_array(Str::lower($level), static::$LEVELS)) {
            $logger::{$level}($data, $context);
        }
    }
}
