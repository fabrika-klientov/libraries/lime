<?php
/**
 * @package   Lime
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Services;

use Lime\Modules\Anemone\AnemoneService;

/**
 * @property \Lime\Modules\Anemone\AnemoneService $anemoneService
 * */
trait Services
{
    /**
     * @param string $name
     * @return mixed
     *
     * @throws \Lime\Exceptions\LimeException
     */
    public function __get($name)
    {
        switch ($name) {
            case 'anemoneService':
                return new AnemoneService($this);
        }

        return null;
    }
}
