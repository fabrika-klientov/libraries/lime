<?php
/**
 * @package   Lime
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Services;

use Lantana\Models\JSSharedCustomers;
use Lime\Contracts\BeModule;
use Lime\Contracts\BeModuleService;
use Lime\Exceptions\LimeException;
use Lime\Modules\Entity;

class ModulesService implements BeModuleService
{
    use Services;

    /**
     * @var JSSharedCustomers|null $customer
     * */
    private $customer;
    /**
     * @var \Illuminate\Support\Collection<BeModule> $modules
     * */
    private $modules;

    /**
     * @param JSSharedCustomers|string|null $customer
     * @return void
     * @throws LimeException
     */
    public function __construct($customer = null)
    {
        if (isset($customer)) {
            $this->init($customer);
        }
    }

    /**
     * @param JSSharedCustomers|string $customer
     * @return $this
     * @throws LimeException
     */
    public function init($customer)
    {
        if (is_string($customer)) {
            $customer = JSSharedCustomers::find($customer);
        }

        if (empty($customer) || !$customer instanceof JSSharedCustomers) {
            $this->customer = null;
            throw new LimeException('Customer not found');
        }

        $this->customer = $customer;

        return $this;
    }

    /**
     * @return bool
     */
    public function isReady()
    {
        return isset($this->customer);
    }

    /**
     * @return JSSharedCustomers|null
     * */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param bool $force
     * @return \Illuminate\Support\Collection<BeModule>
     * */
    public function getModules(bool $force = false)
    {
        if (empty($this->modules) || $force) {
            $this->modules = static::getAvailableModules()->reduce(
                function ($result, $static) {
                    $this->validateModule($static);

                    return $result->merge($static::init($this->customer));
                },
                collect()
            );
        }

        return $this->modules;
    }

    /**
     *
     * @param string $uuidOrId
     * @return BeModule|null
     */
    public function getModule(string $uuidOrId)
    {
        $modules = $this->getModules();
        if ($modules->isEmpty()) {
            return null;
        }

        return $modules->first(
            fn(BeModule $module) => $module->getUUID() == $uuidOrId || $module->getID() == $uuidOrId
        );
    }

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function getEntities()
    {
        return $this->getModules()->map(fn(BeModule $module) => new Entity($module));
    }

    /**
     * @return \Illuminate\Support\Collection
     * */
    public static function statically()
    {
        return static::getAvailableModules()->map(
            fn($class) => ['name' => $class::name(), 'description' => $class::description(), 'icon' => $class::icon()]
        );
    }

    /**
     * @return \Illuminate\Support\Collection
     * */
    public static function getAvailableModules()
    {
        return collect(static::MODULES_AVAILABLE);
    }

    /**
     * @param string $static
     * @return null
     *
     * @throws LimeException
     */
    protected function validateModule($static)
    {
        if (class_exists($static) && is_subclass_of($static, BeModule::class)) {
            return null;
        }

        throw new LimeException('Module ' . $static . ' not found or not implements ' . BeModule::class);
    }
}
