<?php
/**
 * @package   Lime
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Services;

use Lime\Contracts\BeModule;
use Lime\Contracts\BeService;

final class DataStateService implements BeService
{
    private const KEY_DEEP = 'lock_data';
    private const KEY_PULL = 'pull_data';
    /**
     * @var array $MODULES_DATA
     * */
    protected static $MODULES_DATA = [
        'collection' => [
            'anemone' => [
                ['type' => 'leads', 'name' => 'Сделки'],
                ['type' => 'contacts', 'name' => 'Контакты'],
                ['type' => 'companies', 'name' => 'Компании'],
                ['type' => 'customers', 'name' => 'Покупатели'],
            ],
            'crocus' => [
                ['type' => 'orders', 'name' => 'Заказы'],
                ['type' => 'products', 'name' => 'Товары'],
            ],
            'riccia' => [
                ['type' => 'orders', 'name' => 'Заказы'],
                ['type' => 'products', 'name' => 'Товары'],
            ],
        ],
        'injectable' => [
            'anemone' => [
                ['type' => 'hook', 'name' => 'Амо хук (декоратор)'],
            ],
        ],
    ];
    /**
     * @var array $data
     * */
    private $data;
    /**
     * @var \Illuminate\Support\Collection<\Lime\Contracts\BeModule> $modules
     * */
    private $modules;
    /**
     * @var array $anotherRules
     * */
    private array $anotherRules = [];

    /**
     * @param \Illuminate\Support\Collection<\Lime\Contracts\BeModule> $modules
     * @return void
     * */
    public function __construct($modules)
    {
        $this->modules = $modules;
        $this->data = $this->skeletonModulesData();
    }

    /**
     *
     * @param string $module
     * @param string $code
     * @param int $deep
     * @return mixed|null
     */
    public function getForModule(string $module, string $code = null, int $deep = 0)
    {
        $tmp = $this->data;
        while ($deep > 0) {
            $tmp = $tmp[self::KEY_DEEP] ?? null;
            --$deep;
        }

        $data = isset($tmp) ? ($tmp[$module] ?? null) : null;

        return isset($data, $code) ? ($data[$code] ?? null) : $data;
    }

    /**
     *
     * @param string $module
     * @param string $code
     * @param mixed $data
     * @param int $deep
     * @return void
     */
    public function setForModule(string $module, string $code, $data, int $deep = 0)
    {
        $lockData = &$this->getDeepData($deep);

        // deep level
        if ($deep > 0) {
            if (isset($lockData[$module])) {
                $lockData[$module][$code] = $data;
            }
            return;
        }

        // top level
        if (isset($this->data[$module])) {
            $this->data[$module][$code] = $data;
        }
    }

    /**
     * @param string $module
     * @param string|null $code
     * @param int $deep
     * @return mixed|null
     */
    public function getPullDataForModule(string $module, string $code = null, int $deep = 0)
    {
        $pullData = $this->getForModule(self::KEY_PULL, $module, $deep);

        if (!isset($pullData)) { // pull not init
            return null;
        }

        return isset($code) ? ($pullData[$code] ?? null) : $pullData;
    }

    /**
     * @param string $module
     * @param string $code
     * @param $data
     * @param int $deep
     * @return void
     */
    public function setPullDataForModule(string $module, string $code, $data, int $deep = 0)
    {
        $pullData = $this->getPullDataForModule($module, null, $deep);
        if (!isset($pullData)) { // pull not init
            return;
        }

        $newPullData = array_merge($pullData, [$code => $data]);

        $this->setForModule(self::KEY_PULL, $module, $newPullData, $deep);
    }

    /**
     * @param int $deep
     * @return void
     */
    public function initDeepData(int $deep)
    {
        while ($deep > 0) {
            if (!isset($this->data[self::KEY_DEEP])) {
                $this->data[self::KEY_DEEP] = $this->skeletonModulesData();
            } elseif (!isset($lockData[self::KEY_DEEP])) {
                $lockData[self::KEY_DEEP] = $this->skeletonModulesData();
            }

            if (isset($lockData)) {
                $lockData = &$lockData[self::KEY_DEEP];
            } else {
                $lockData = &$this->data[self::KEY_DEEP];
            }

            --$deep;
        }
    }

    /**
     * @param int $deep
     * @return void
     */
    public function destroyDeepData(int $deep)
    {
        if ($deep <= 0) {
            return;
        }

        if ($deep > 1) {
            $lockData = &$this->getDeepData($deep - 1);
        } else {
            $lockData = &$this->data;
        }

        if (isset($lockData)) {
            unset($lockData[self::KEY_DEEP]);
        }
    }

    /**
     * @param int $deep
     * @return void
     */
    public function initPullData(int $deep)
    {
        if ($deep > 0) {
            $lockData = &$this->getDeepData($deep);
        } else {
            $lockData = &$this->data;
        }

        if (isset($lockData)) {
            $lockData[self::KEY_PULL] = $this->skeletonModulesData();
        }
    }

    /**
     * @param int $deep
     * @return void
     */
    public function destroyPullData(int $deep)
    {
        if ($deep > 0) {
            $lockData = &$this->getDeepData($deep);
        } else {
            $lockData = &$this->data;
        }

        if (isset($lockData[self::KEY_PULL])) {
            unset($lockData[self::KEY_PULL]);
        }
    }

    /**
     * @param string $uuid
     * @return bool
     */
    public function hasAnotherRule(string $uuid)
    {
        return in_array($uuid, $this->anotherRules);
    }

    /**
     * @param string $uuid
     * @return void
     */
    public function addAnotherRule(string $uuid)
    {
        $this->anotherRules[] = $uuid;
    }

    /**
     * @param string $uuid
     * @return void
     */
    public function removeAnotherRule(string $uuid)
    {
        array_splice($this->anotherRules, array_search($uuid, $this->anotherRules), 1);
    }

    /**
     * @param int $deep
     * @return mixed|null
     */
    protected function &getDeepData(int $deep)
    {
        $null = null;
        while ($deep > 0) {
            if (!isset($this->data[self::KEY_DEEP]) || (isset($lockData) && !isset($lockData[self::KEY_DEEP]))) {
                return $null;
            }

            if (isset($lockData)) {
                $lockData = &$lockData[self::KEY_DEEP];
            } else {
                $lockData = &$this->data[self::KEY_DEEP];
            }

            --$deep;
        }

        if (isset($lockData)) {
            return $lockData;
        }

        return $null;
    }

    /**
     * @return array
     * */
    protected function skeletonModulesData(): array
    {
        return $this->modules->reduce(
            fn($result, BeModule $module) => array_merge($result, [$module->getUUID() => []]),
            []
        );
    }

    /**
     * @param \Closure|null $closure
     * @return mixed
     */
    public function list(\Closure $closure = null)
    {
        if (isset($closure)) {
            return $closure($this->data);
        }

        return $this->data;
    }

    /**
     * @param string|null $uuid
     * @return \Illuminate\Support\Collection<\Lime\Contracts\BeModule>|\Lime\Contracts\BeModule
     */
    public function getModules(string $uuid = null)
    {
        return isset($uuid)
            ? $this->modules->first(fn(BeModule $module) => $module->getUUID() == $uuid)
            : $this->modules;
    }

    /**
     * @return array
     * */
    public static function getDataSkeleton()
    {
        return static::$MODULES_DATA;
    }
}
