<?php
/**
 * @package   Lime
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Contracts;

use Lantana\Models\JSSharedCustomers;
use Lime\Services\DataStateService;

interface BeModule
{
    /**
     * @return string
     * */
    public static function name(): string;

    /**
     * @return string
     * */
    public static function description(): string;

    /**
     * @return string
     * */
    public static function icon(): string;

    /**
     * @return bool
     * */
    public function getPower(): bool;

    /**
     * @param bool $power
     * @return void
     * */
    public function setPower(bool $power): void;

    /**
     * @return string|mixed
     * */
    public function getID();

    /**
     * @return string
     * */
    public function getUUID();

    /**
     * @return string
     * */
    public function getCustomerUUID();

    /**
     * @return mixed
     * */
    public function getClient();

    /**
     * @param JSSharedCustomers $customer
     * @return \Illuminate\Support\Collection
     */
    public static function init(JSSharedCustomers $customer);

    /**
     * @param string|null $type enum in[condition | action]
     * @param mixed|null $context
     * @return \Illuminate\Support\Collection
     */
    public static function statically(string $type = null, $context = null);

    /**
     * @param DataStateService $dataStateService
     * @param mixed $bottomValue
     * @param array $injectDeep
     * @return mixed
     */
    public function getValueFor(DataStateService $dataStateService, $bottomValue, array $injectDeep);

    /**
     * @param DataStateService $dataStateService
     * @return BeConverting
     */
    public function getConverting(DataStateService $dataStateService): BeConverting;

    /** config for front (dynamic data for module)
     * @return array
     * */
    public function fullDynamically(): array;
}
