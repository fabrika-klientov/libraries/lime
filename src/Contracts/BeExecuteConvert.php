<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.04
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Contracts;

use Lantana\Models\JSLimeConvert;

interface BeExecuteConvert
{
    /**
     * @param JSLimeConvert $limeConvert
     * @param null $additional
     * @return mixed
     */
    public function handle(JSLimeConvert $limeConvert, $additional = null);
}
