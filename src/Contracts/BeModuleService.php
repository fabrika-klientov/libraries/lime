<?php
/**
 * @package   Lime
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Contracts;

interface BeModuleService extends BeService
{
    const MODULES_AVAILABLE = [
        \Lime\Modules\Lime\Module::class,
        \Lime\Modules\Anemone\Module::class,
        \Lime\Modules\Black\Module::class,
        \Lime\Modules\Crocus\Module::class,
        \Lime\Modules\Riccia\Module::class,
    ];
}
