<?php
/**
 * @package   Lime
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Contracts;

use Lime\Services\DataStateService;
use Lime\Services\ModulesService;

interface BeCondact
{

    /**
     * @param DataStateService $dataStateService
     * @param ModulesService $modulesService
     */
    public function __construct(DataStateService $dataStateService, ModulesService $modulesService);

    /**
     * @return string
     * */
    public static function module(): string;

    /**
     * @return string
     * */
    public static function name(): string;


    /** config for front
     * @return array
     * */
    public static function statically(): array;

    /** config for front (dynamic data key|entity)
     * @return string
     * */
    public static function forDynamically(): ?string;

}
