<?php
/**
 * @package   Lime
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Contracts;

interface BeCondition extends BeCondact
{
    const TYPE = 'condition';

    /**
     * @param mixed $inner
     * @param array $deep
     * @return bool
     */
    public function handle($inner = null, array $deep = []): bool;
}
