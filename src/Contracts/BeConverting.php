<?php
/**
 * @package   Lime
 * @category  Anemone
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.04
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Contracts;

interface BeConverting
{
    /** for getting converted data
     * @param mixed $bottomValue
     * @param array $deep
     * @return mixed
     */
    public function getValue($bottomValue, array $deep);

    /** for get prepare data
     * @param mixed $bottomValue
     * @param array $deep
     * @return mixed
     * @throws \Exception
     */
    public function getPreparingData($bottomValue, array $deep);

    /**
     * @param string $code
     * @return BeExecuteConvert|null
     */
    public function getConvert(string $code): ?BeExecuteConvert;
}
