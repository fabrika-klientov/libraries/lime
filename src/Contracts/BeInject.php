<?php
/**
 * @package   Lime
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Contracts;

use Lime\Services\DataStateService;

interface BeInject
{
    /**
     * @param DataStateService $dataStateService
     * @return void|mixed
     */
    public function up(DataStateService $dataStateService);

    /**
     * @param DataStateService $dataStateService
     * @return void|mixed
     */
    public function down(DataStateService $dataStateService);

}
