<?php
/**
 * @package   Lime
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Contracts;

interface BeAction extends BeCondact
{
    const TYPE = 'action';

    /**
     * @param mixed $inner
     * @param array $deep
     * @return mixed|null|void
     */
    public function handle($inner, array $deep = []);
}
