<?php
/**
 * @package   Lime
 * @category  Exceptions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.05
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Exceptions;

class LimeStatusException extends \Exception
{
    public const CONTINUE_COLLECT_ACT = 1001;
    public const BREAK_COLLECT_ACT = 1002;
}
