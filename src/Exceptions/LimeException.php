<?php
/**
 * @package   Lime
 * @category  Exceptions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lime\Exceptions;

class LimeException extends \Exception
{

}
